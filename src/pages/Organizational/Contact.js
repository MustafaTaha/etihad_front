import React from 'react';
import Header from '../../Components/Common/Header';
import Footer from '../../Components/Common/Footer';


import { useTranslation } from 'react-i18next';



const Contact = () => {
    const { t } = useTranslation();

    return (
        <>
            <Header />
            <main>
                <section className="bread_crumb">
                    <div className="container">
                        <h3 className="title">{t("Contact us")}</h3>
                    </div>
                </section>

                <section className="our_projects contact_us p-t-b">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-9">
                                <div className="sec_title" data-aos="fade-up">
                                    <h4 className="sub_title">{t("GET IN TOUCH")}</h4>
                                    <h3 className="title">
                                        {t("Send us a message")}
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-8">
                                <form action="#" class="proForm" data-aos="fade-up">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="control-label" for="First_name">First name</label>
                                                <input type="text" id="First_name" class="form-control" placeholder="Your" />
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="control-label" for="Last_name">Last name</label>
                                                <input type="text" id="Last_name" class="form-control" placeholder="Name" />
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="control-label" for="Mail">Mail</label>
                                                <input type="email" id="Mail" class="form-control" placeholder="Your Email" />
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="form-group">
                                                <label class="control-label" for="Phone">Phone</label>
                                                <input type="text" id="Phone" class="form-control" placeholder="+880" />
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <label class="control-label" for="Massage">Massage</label>
                                                <textarea id="Massage" class="form-control details"
                                                    placeholder="Type your massage here..."></textarea>
                                            </div>
                                        </div>
                                        <div class="col-12">
                                            <div class="form-group">
                                                <button class="btn shared">
                                                    <span>Send</span>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="col-lg-4">
                                <div class="contact_Info">
                                    <h4 class="title">Contact Info</h4>
                                    <div class="social">
                                        <ul class="list-unstyled">
                                            <li>
                                                <a target="_blank" href="https://www.youtube.com/channel/UCpXCNb-HmlKfNHfSiE4CIeA"><i className="fab fa-youtube"></i></a>
                                            </li>
                                            <li>
                                                <a target="_blank" href="https://www.snapchat.com/add/miecajman"><i className="fab fa-snapchat-square"></i></a>
                                            </li>
                                            <li>
                                                <a target="_blank" href="https://wa.me/971566644277"><i className="fab fa-whatsapp"></i></a>
                                            </li>
                                            <li>
                                                <a target="_blank" href="https://twitter.com/etihadcharity/status/1388782686353887233"><i className="fab fa-twitter"></i></a>
                                            </li>
                                            <li>
                                                <a target="_blank" href="https://www.instagram.com/etihadcharity/"><i className="fab fa-instagram"></i></a>
                                            </li>
                                            <li>
                                                <a target="_blank" href="https://www.facebook.com/etihadcharity/"><i className="fab fa-facebook"></i></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="details">
                                        <p class="info">
                                            <span>Location :</span>
                                            <a class="link" href="#" target="_blank" rel="noopener noreferrer">Sheikh Maktoum Bin
                                                Rashid Street, Al Jurf 2, Ajman, United Arab Emirates </a>
                                        </p>
                                        <p class="info">
                                            <span>Email : </span>
                                            <a class="link" href="mailto:info@etihadcharity.ae"> info@etihadcharity.ae</a>
                                        </p>
                                        <p class="info">
                                            <span>Phone : </span>
                                            <a class="link" href="tel:+971 56 664 4277"> +971 56 664 4277</a>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </section>

            </main>
            <Footer />
        </>
    );
}

export default Contact;