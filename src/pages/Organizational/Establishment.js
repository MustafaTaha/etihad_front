import React from 'react';
import Header from '../../Components/Common/Header';
import Footer from '../../Components/Common/Footer';

import timline from '../../styles/assets/images/timline.jpg';

import { useTranslation } from 'react-i18next';



const Establishment = () => {
    const { t } = useTranslation();

    return (
        <>
            <Header />
            <main>
                <section className="bread_crumb">
                    <div className="container">
                        <h3 className="title">{t("Establishment")}</h3>
                    </div>
                </section>

                <section className="our_projects medi_center p-t-b">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-9">
                                <div className="sec_title" data-aos="fade-up">
                                    <h4 className="sub_title">{t("Establishment")}</h4>
                                    <h3 className="title">
                                        {t("We believe we can save more lives with you. Good organization is one of the most important secrets of our success:")}
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div className="charity_structure" data-aos="fade-up">
                            <img src={timline} className="img-fluid" alt="structure" />
                        </div>

                    </div>
                </section>

            </main>
            <Footer />
        </>
    );
}

export default Establishment;