import React from 'react';
import Header from '../../Components/Common/Header';
import Footer from '../../Components/Common/Footer';
 

import { useTranslation } from 'react-i18next';



const Structure = () => {
    const { t } = useTranslation();

    return (
        <>
            <Header />
            <main>
                <section className="bread_crumb">
                    <div className="container">
                        <h3 className="title">{t("THE WORD OF THE BOARD OF DIRECTORS")}</h3>
                    </div>
                </section>

                <section className="our_projects medi_center p-t-b">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-12">
                                <div className="sec_title" data-aos="fade-up">
                                    <h4 className="sub_title">{t("الرئيس التنفيذي لمؤسسة الاتحاد الخيرية في عجمان")}</h4>
                                    <h3 className="title">{t("كلمة الشيخ علي بن محمد بن علي  النعيمي")} </h3>
                                </div>
                                <div>
                                    <p className='' style={{lineHeight:"46px", fontSize:"20px",color:"#FFF"}}>
                                        على مدار سنوات طويلة من العمل الإنساني المخلص والجاد لهذا الصرح الإنساني الوطني، استطعت الأجيال المتعاقبة في هذه مؤسسة الاتحاد الخيرية ان تسطر سجلا حافلا من الإنجازات التي نتشرف بها، ونسعد انا نكون جزء منها.

                                        ولقد كونت مؤسستنا نموذجا متميزا في العمل الإنساني، وقدمت تجربة فريدة في الاهتمام بالتعليم المجاني.

                                        لقد شيدت مؤسسة الاتحاد الخيرية، مدرستين للبنين والبنات، تخرج منهما آلاف الطلاب من مشاعل العلم لصناعة جيل جديد يتسلح بالمعرفة والعلم والولاء والحب للإمارات التي اعطتهم كل ما يحتاجون وما يتمنون.

                                        وانجزت مؤسستنا العريقة في السنوات الماضية مشاريع تنموية وإنسانية كبيرة، في مختلف المجالات، أبرزها حفر الابار السطحية وبناء مراكز تحفيظ القرآن وتوفير المياه وبناء المساجد وكذلك تجهيز منزل وتركيب برادات المياه، ودعم الاسر المنتجة، بالإضافة الى تيسير الزواج.

                                        فلمؤسستنا العريقة، بصمات كثيرة وعلامات مضيئة في كثير من البقاع والاصقاع، لنكون في طليعة مؤسسات الخير من دولة زايد الخير، الذي شمل عطاءه القاصي والداني.
                                        لقد كانت وستظل مؤسستنا، نبراسا للعمل الخيري ونشر النور والعلم والتفاؤل وقيم التكافل الاجتماعي وتعزيز المحبة في المجتمع والولاء لقيادتنا الرشيدة، وذلك بدعمكم ووجود هذه النخبة المتميزة المحبة للعطاء والعاملة له وبه.

                                    </p>
                                </div>
                            </div>
                        </div>


                    </div>
                </section>

            </main>
            <Footer />
        </>
    );
}

export default Structure;