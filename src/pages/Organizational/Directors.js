import React from 'react';

import Header from '../../Components/Common/Header';
import Footer from '../../Components/Common/Footer';

import Chairman from '../../styles/assets/images/Chairman.jpeg';
import vice from '../../styles/assets/images/vice.jpeg';
import Member from '../../styles/assets/images/Member.jpeg';
import ceo from '../../styles/assets/images/ceo.jpeg';
import Executive from '../../styles/assets/images/Executive.jpeg';
import Member2 from '../../styles/assets/images/Member2.jpeg';
import Member3 from '../../styles/assets/images/Member3.jpeg';


import { useTranslation } from 'react-i18next';



const Directors = () => {
    const { t } = useTranslation();

    return (
        <>
            <Header />
            <main>
                <section className="bread_crumb">
                    <div className="container">
                        <h3 className="title">{t("Board of Trustees and Senior Management")}</h3>
                    </div>
                </section>

                <section className="our_projects medi_center p-t-b">
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-lg-10">
                                <div className="row">
                                    <div className="col-lg-9">
                                        <div className="sec_title" data-aos="fade-up">
                                            <h4 className="sub_title">{t("happy to be with you")}</h4>
                                            <h3 className="title">
                                                {t("With Your Help We Can Do More & More")}
                                            </h3>
                                            <p className="desc mt-2">
                                                {t("The Board of Trustees of the Union Charitable Foundation and the senior management includes a group of statesmen who have connections and a long history in administrative leadership and charitable work at the level of the local governmental and charitable institutions of the Emirate of Ajman. It consists of:")}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div className="row justify-content-center ">
                                    <div className=" col-lg-5 col-md-6">
                                        <div className="donaite-card board_card " data-aos="fade-up">
                                            <p className="donaite-image">
                                                <img src={Chairman} alt="Chairman" className="img-fluid" />
                                            </p>
                                            <div className="text">
                                                {/* <div className="details">
                                                    <p className="num">24000+</p>
                                                </div> */}
                                                <span className="ch_name">{t("Sheikh Mohammed bin Ali bin Rashid Al Nuaim")}</span>
                                                <p className="name">{t("Chairman of the Board of Trustees")}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="row justify-content-center">
                                    <div className="col-md-6">
                                        <div className="donaite-card board_card " data-aos="fade-up">
                                            <p className="donaite-image">
                                                <img src={vice} alt="vice" className="img-fluid" />
                                            </p>
                                            <div className="text">
                                                <span className="ch_name">{t("Sheikh Rashid bin Mohammed bin Ali Al Nuaimi")}</span>
                                                <p className="name">{t("Vice President of the Board of Trustees")}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="donaite-card board_card " data-aos="fade-up">
                                            <p className="donaite-image">
                                                <img src={Member3} alt="Member" className="img-fluid" />
                                            </p>
                                            <div className="text">
                                                <span className="ch_name">{t("Abdullah Mohammed Al Muwaiji")}</span>
                                                <p className="name">{t("Member of the Board of Trustees")}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="donaite-card board_card " data-aos="fade-up">
                                            <p className="donaite-image">
                                                <img src={Member} alt="Member" className="img-fluid" />
                                            </p>
                                            <div className="text">
                                                <span className="ch_name">{t("Hareb Salem Al-Erian")}</span>
                                                <p className="name">{t("Secretary General")}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="donaite-card board_card " data-aos="fade-up">
                                            <p className="donaite-image">
                                                <img src={Member2} alt="Member" className="img-fluid" />
                                            </p>
                                            <div className="text">
                                                <span className="ch_name">{t("Rashid Abdullah Al Shamsi")}</span>
                                                <p className="name">{t("Board of Trustees members")}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="donaite-card board_card " data-aos="fade-up">
                                            <p className="donaite-image">
                                                <img src={ceo} alt="ceo" className="img-fluid" />
                                            </p>
                                            <div className="text">
                                                <span className="ch_name">{t("Nasser Ali Al-Junaibi")}</span>
                                                <p className="name">{t("Executive Director")}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-md-6">
                                        <div className="donaite-card board_card " data-aos="fade-up">
                                            <p className="donaite-image">
                                                <img src={Executive} alt="Executive" className="img-fluid" />
                                            </p>
                                            <div className="text">
                                                <span className="ch_name">{t("Sheikh Ali bin Muhammad bin Ali Al Nuaimi")}</span>
                                                <p className="name">{t("chief executive officer")}</p>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                </div>
                            </div>
                        </div>


                    </div>
                </section>

            </main>
            <Footer />
        </>
    );
}

export default Directors;