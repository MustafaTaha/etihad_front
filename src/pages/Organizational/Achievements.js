import React from 'react';

import Header from '../../Components/Common/Header';
import Footer from '../../Components/Common/Footer';

import donaite from '../../styles/assets/images/donaite2.png';
import achevment from '../../styles/assets/images/achevment.jpg'; 



import { useTranslation } from 'react-i18next';



const Achievements = () => {
    const { t } = useTranslation();
    return (
        <>
            <Header />
            <main>
                <section className="bread_crumb">
                    <div className="container">
                        <h3 className="title">{t("Our achievements")}</h3>
                    </div>
                </section>

                <section className="our_projects medi_center p-t-b">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-9">
                                <div className="sec_title" data-aos="fade-up">
                                    <h4 className="sub_title">{t("Our achievements")}</h4>
                                    <h3 className="title">
                                        {t("We believe we can save more lives with you. Good organization is one of the most important secrets of our success:")}
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div className="charity_structure" data-aos="fade-up">
                            <img src={achevment} className="img-fluid" alt="achevment" />
                        </div>

                    </div>
                </section>

            </main>
            <Footer />
        </>
    );
}

export default Achievements;