import React, { useEffect, useState } from 'react';
import { Card, CardBody, Col, Container, Input, Label, Row, Button, Form, FormFeedback, Alert, Spinner } from 'reactstrap';
import ParticlesAuth from "../AuthenticationInner/ParticlesAuth";


//redux
import { useSelector, useDispatch } from "react-redux";

import { Link, redirect, useNavigate } from "react-router-dom";
import withRouter from "../../Components/Common/withRouter";
// Formik validation
import * as Yup from "yup";
import { useFormik } from "formik";

// actions
import { userChangePassword } from "../../slices/thunks";
 
//import images
import login_imag from '../../styles/assets/images/login_imag.png';
import logo from '../../styles/assets/images/Logo.svg';
import pattern_top from '../../styles/assets/images/pattern_top.svg';
import pattern_bottom from '../../styles/assets/images/pattern_bottom.svg';
import facebook from '../../styles/assets/images/faccebook.svg';
import google from '../../styles/assets/images/google.svg';
import { toast } from 'react-toastify';
import { useTranslation } from 'react-i18next';
import { LoginSocialGoogle } from 'reactjs-social-login';

import { setIsSuccess_CP } from '../../slices/auth/ChangePassword/reducer';

const ChangePassword = (props) => {
    const { t } = useTranslation();
    const navigate = useNavigate();
    const history = useNavigate();
    const dispatch = useDispatch();
    useEffect(() => {
        if (user?.id) {
            // console.log(user);
        } else {
            navigate("/");
            toast.warn(t("You don't have permission to access this page"));
        }
    }, []);
    const { user, error, loading, errorMsg, success } = useSelector(state => ({
        user: state.ForgetPassword.user,
        success: state.ChangePassword.success,
        // error: state.Login.error,
        // loading: state.Login.loading,
        // errorMsg: state.Login.errorMsg,
    }));

    const [showPassword, setShowPassword] = useState(false);
    const [showConfirmPassword, setShowConfirmPassword] = useState(false);

    useEffect(() => {
        error ? toast.error(error) : null
    }, [error])

    useEffect(() => {
        if (success) {
            toast.success(t("Password Changed Successfully"))
            setTimeout(() => history("/login"), 500);
            dispatch(setIsSuccess_CP(false))
        }

        // setTimeout(() => {
        //   dispatch(resetRegisterFlag());
        // }, 3000);

    }, [success]);

    const validation = useFormik({
        // enableReinitialize : use this flag when initial values needs to be changed
        enableReinitialize: true,

        initialValues: {
            password: '',
            password_confirmation: '',
        },
        validationSchema: Yup.object({
            password: Yup.string().required(t("Please Enter Your Password")).min(8, t("password must be at least 8 characters")),
            password_confirmation: Yup.string()
                .required(t("Please Enter Password Confirmation"))
                .min(8)
                .when("password", (password, field) => {
                    if (password) {
                        return field.required(t("Please Enter Password Confirmation")).oneOf([Yup.ref("password")], t("Password didn't match"));
                    }
                }),
        }),
        onSubmit: (values) => {
            let obj = {
                password: values.password,
                id: user?.id
            }
            dispatch(userChangePassword(obj, props.router.navigate));
        }
    });







    return (
        <ParticlesAuth>
            <section className="login_page">
                <img src={pattern_top} className="img-fluid pattern top" alt="pattern" />
                <img src={pattern_bottom} className="img-fluid pattern bottom" alt="pattern" />
                <Container>
                    <div className="logo">
                        <Link to="/" href="index.html">
                            <img src={logo} className="img-fluid" alt="logo" />
                        </Link>
                    </div>
                    <Row>
                        <Col lg="8">
                            <div className="img">
                                <img src={login_imag} className="img-fluid" alt="image" />
                            </div>
                        </Col>
                        <Col lg="4">
                            <Form className="proForm"
                                onSubmit={(e) => {
                                    e.preventDefault();
                                    validation.handleSubmit();
                                    return false;
                                }}
                                action="#">
                                <div className="text">
                                    <div className="row">
                                        <div className="col-12">
                                            <h4 className="title">{t("Reset Password")}</h4>
                                            <p className="desc">
                                                {t("With your effort, you can make a difference in the lives of those in need. Join us today to help create a better world for all!")}
                                            </p>
                                            <p className="havAcc">{t("Don’t have account yet")}? <Link to="/register" className='link'>{t("Signup")}</Link>  </p>
                                        </div>

                                        <div className="col-12">
                                            <div className="form-group">
                                                <Input
                                                    name="password"
                                                    value={validation.values.password || ""}
                                                    type={showPassword ? "text" : "password"}
                                                    className="form-control"
                                                    placeholder={t("Enter Password")}
                                                    onChange={validation.handleChange}
                                                    onBlur={validation.handleBlur}
                                                    invalid={
                                                        validation.touched.password && validation.errors.password ? true : false
                                                    }
                                                />
                                                {validation.touched.password && validation.errors.password ? (
                                                    <FormFeedback type="invalid">{validation.errors.password}</FormFeedback>
                                                ) : null}
                                            </div>
                                        </div>
                                        <div className="col-12" >
                                            <div className="form-group">
                                                <Input
                                                    name="password_confirmation"
                                                    id="password_confirmation-field"
                                                    className="form-control"
                                                    placeholder={t("Enter Password Confirmation")}
                                                    autoComplete="new-password"
                                                    type={showConfirmPassword ? "text" : "password"}
                                                    validate={{
                                                        required: { value: true },
                                                    }}
                                                    onChange={validation.handleChange}
                                                    onBlur={validation.handleBlur}
                                                    value={validation.values.password_confirmation || ""}
                                                    invalid={validation.touched.password_confirmation && validation.errors.password_confirmation ? true : false}
                                                />
                                                {validation.touched.password_confirmation && validation.errors.password_confirmation ? <FormFeedback type="invalid">{t(validation.errors.password_confirmation)}</FormFeedback> : null}

                                            </div>
                                        </div>
                                        <div className="col-12">
                                            <button className="btn submit"  >
                                                {loading ? <Spinner size="sm" className='me-2'> Loading... </Spinner> : null}
                                                {t("Submit")}
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </Form>

                        </Col>
                    </Row>


                </Container>
            </section>
        </ParticlesAuth>
    );
};

export default withRouter(ChangePassword);