import React, { useEffect } from "react";
import { Row, Col, Modal, ModalBody, ModalHeader, Container, Input, Label, Form, FormFeedback, Spinner } from "reactstrap";

// Formik Validation
import * as Yup from "yup";
import { useFormik } from "formik";

import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

// action
import { registerUser, checkEmailExist, checkOTB, apiError, resetRegisterFlag, checkPhoneExist } from "../../slices/thunks";

//redux
import { useSelector, useDispatch } from "react-redux";

import { Link, useNavigate } from "react-router-dom";

import ParticlesAuth from "../AuthenticationInner/ParticlesAuth";
//import images
import login_imag from '../../styles/assets/images/login_imag.png';
import logo from '../../styles/assets/images/Logo.svg';
import pattern_top from '../../styles/assets/images/pattern_top.svg';
import pattern_bottom from '../../styles/assets/images/pattern_bottom.svg';
import facebook from '../../styles/assets/images/faccebook.svg';
import google from '../../styles/assets/images/google.svg';
import { useTranslation } from "react-i18next";
import Select from "react-select";
import _ from "lodash";
import { useState } from "react";
import { useCallback } from "react";
import { emailExist, phoneExist } from "../../slices/auth/register/reducer";

const countryCodes = require('country-codes-list');
const myCountryCodesObject = countryCodes.customList('countryCode', '+{countryCallingCode}');
const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

const Register = (props) => {
    const { t } = useTranslation();

    const history = useNavigate();
    const dispatch = useDispatch();

    const [registerObj, setRegisterObj] = useState({});
    const { OTB_number } = useSelector((state) => ({
        OTB_number: state.Register.otb,
    }));
    // console.log(OTB_number);
    const validation = useFormik({
        // enableReinitialize : use this flag when initial values needs to be changed
        enableReinitialize: true,
        validateOnChange: true,
        initialValues: {
            email_id: '',
            // first_name: '',
            // last_name: '',
            full_name: '',
            phone_number: '',
            password: '',
            confirm_password: '',
            country_code: ''
        },
        validationSchema: Yup.object({
            email_id: Yup.string().email(t("Please Enter Vaild Email")).required(t("Please Enter Your Email")),
            // first_name: Yup.string().required(t("Please Enter Your first name")),
            // last_name: Yup.string().required(t("Please Enter Your last name")),
            full_name: Yup.string().required(t("Please Enter Your Name")),
            password: Yup.string().required(t("Please Enter Your Password")).min(8, t("password must be at least 8 characters")),
            phone_number: Yup.string().required(t("Please Enter Your phone number")).matches(phoneRegExp, t('Phone number is not valid')),
            country_code: Yup.string().required(t("Please Select Country Code")),
        }),

        onSubmit: (values) => {
            showOTBPopup();
            let obj = {
                screen: "change_password",
                name: values.full_name,
                mobile_number: values.country_code + values.phone_number,
            }

            setRegisterObj({ ...values, social_media_name: "own" });
            dispatch(checkOTB(obj))
        }
    });
    const checkEmail = (e) => {
        dispatch(checkEmailExist(e))
    }
    const checkPhone = (phone_number, country_code) => {
        dispatch(checkPhoneExist(phone_number, country_code))
    }
    const validationOTB = useFormik({
        // enableReinitialize : use this flag when initial values needs to be changed
        enableReinitialize: true,

        initialValues: {
            otb: '',
        },
        validationSchema: Yup.object({
            otb: Yup.number().typeError(t("otb must be a number")).required(t("Please Enter OTB number"))
        }),
        onSubmit: (values) => {
            values.otb == OTB_number ? dispatch(registerUser(registerObj)) : toast.error("OTB is invaild")
        }
    });

    /*popup for enter OTB*/
    const [OTBPopup, setOTBPopup] = useState(false);

    const showOTBPopup = useCallback((item) => {
        if (OTBPopup) {
            setOTBPopup(false);
            validationOTB.resetForm();
        } else {
            setOTBPopup(true);
        }
    }, [OTBPopup]);

    const { error, success, registrationError, message, loading, isEmailExist, isPhoneExist } = useSelector(state => ({
        success: state.Register.success,
        error: state.Register.error,
        loading: state.Register.loading,
        registrationError: state.Register.registrationError,
        message: state.Register.response?.data?.message,
        isEmailExist: state.Register.emailExist,
        isPhoneExist: state.Register.phoneExist
    }));

    useEffect(() => {
        if (isEmailExist) {
            toast.error(t("Failed! User already exists with the provided email address"))
            validation.setFieldError("email_id", t("User already exists with the provided email address"))
            validation.setFieldValue("email_id", "")
            dispatch(emailExist(false))
        }
    }, [isEmailExist])

    useEffect(() => {
        if (isPhoneExist) {
            toast.error(t("Failed! User already exists with the provided phone number"))
            validation.setFieldError("phone_number", t("User already exists with the provided phone number"))
            validation.setFieldValue("phone_number", "")
            dispatch(phoneExist(false))
        }
    }, [isPhoneExist])

    useEffect(() => {
        message ? toast.error(message) : null
    }, [message])

    useEffect(() => {
        dispatch(apiError(""));
    }, [dispatch]);

    useEffect(() => {
        if (success) {
            toast.success(t("account created successfully"))
            setTimeout(() => history("/login"), 500);
        }

        // setTimeout(() => {
        //     dispatch(resetRegisterFlag());
        // }, 3000);

    }, [dispatch, success, error, history]);
    return (
        <>
            <ParticlesAuth>
                <section className="login_page">
                    <img src={pattern_top} className="img-fluid pattern top" alt="pattern" />
                    <img src={pattern_bottom} className="img-fluid pattern bottom" alt="pattern" />
                    <Container>
                        <div className="logo">
                            <Link to="/" href="index.html">
                                <img src={logo} className="img-fluid" alt="logo" />
                            </Link>
                        </div>
                        <Row>
                            <Col lg="8">
                                <div className="img">
                                    <img src={login_imag} className="img-fluid" alt="image" />
                                </div>
                            </Col>
                            <Col lg="4">
                                <Form className="proForm"
                                    onSubmit={(e) => {
                                        e.preventDefault();
                                        validation.handleSubmit();
                                        return false;
                                    }}
                                    action="#">
                                    <div className="text">
                                        <div className="row">
                                            <Col lg="12">
                                                <h4 className="title">{t("Signup")}</h4>
                                                <p className="desc">
                                                    {t("With your effort, you can make a difference in the lives of those in need. Join us today to help create a better world for all!")}
                                                </p>
                                                <p className="havAcc">{t("Alredy have account")} ? <Link to="/login" className='link'>{t("Login")}</Link>  </p>
                                            </Col>
                                            {/* <Col lg="6">
                                                <div className="form-group">
                                                    <Input
                                                        name="first_name"
                                                        className="form-control"
                                                        placeholder={t("First name")}
                                                        type="text"
                                                        onChange={validation.handleChange}
                                                        onBlur={validation.handleBlur}
                                                        value={validation.values.first_name || ""}
                                                        invalid={
                                                            validation.touched.first_name && validation.errors.first_name ? true : false
                                                        }
                                                    />

                                                    {validation.touched.first_name && validation.errors.first_name ? (
                                                        <FormFeedback type="invalid">{validation.errors.first_name}</FormFeedback>
                                                    ) : null}
                                                </div>
                                            </Col>
                                            <Col lg="6">
                                                <div className="form-group">
                                                    <Input
                                                        name="last_name"
                                                        className="form-control"
                                                        placeholder={t("Last name")}
                                                        type="text"
                                                        onChange={validation.handleChange}
                                                        onBlur={validation.handleBlur}
                                                        value={validation.values.last_name || ""}
                                                        invalid={
                                                            validation.touched.last_name && validation.errors.last_name ? true : false
                                                        }
                                                    />

                                                    {validation.touched.last_name && validation.errors.last_name ? (
                                                        <FormFeedback type="invalid">{validation.errors.last_name}</FormFeedback>
                                                    ) : null}
                                                </div>
                                            </Col> */}
                                            <Col lg={12}>
                                                <div className="form-group">
                                                    <Input
                                                        name="full_name"
                                                        className="form-control"
                                                        placeholder={t("Name")}
                                                        type="text"
                                                        onChange={validation.handleChange}
                                                        onBlur={validation.handleBlur}
                                                        value={validation.values.full_name || ""}
                                                        invalid={
                                                            validation.touched.full_name && validation.errors.full_name ? true : false
                                                        }
                                                    />

                                                    {validation.touched.full_name && validation.errors.full_name ? (
                                                        <FormFeedback type="invalid">{validation.errors.full_name}</FormFeedback>
                                                    ) : null}
                                                </div>
                                            </Col>
                                            <Col lg="12">
                                                <div className="form-group">
                                                    <Input
                                                        name="email_id"
                                                        className="form-control"
                                                        placeholder={t("Email adress")}
                                                        type="text"
                                                        autoComplete="new-password"
                                                        onChange={validation.handleChange}
                                                        onBlur={(e) => {
                                                            validation.handleBlur(e)
                                                            if (e.target.value) {
                                                                checkEmail(e.target.value)
                                                            }
                                                        }}
                                                        value={validation.values.email_id || ""}
                                                        invalid={
                                                            validation.touched.email_id && validation.errors.email_id ? true : false
                                                        }
                                                    />

                                                    {validation.touched.email_id && validation.errors.email_id ? (
                                                        <FormFeedback type="invalid">{validation.errors.email_id}</FormFeedback>
                                                    ) : null}
                                                </div>
                                            </Col>
                                            <Col lg={12}>
                                                <div className="form-group select">
                                                    <Select
                                                        placeholder={t("Country Code")}
                                                        value={_.values(myCountryCodesObject)?.map((country_code) => ({ label: country_code, value: country_code }))?.find((country_code) => country_code.value === validation.values.country_code) || ""}
                                                        onChange={(e) => {
                                                            validation.setFieldValue("country_code", e?.value);
                                                        }}
                                                        options={_.values(myCountryCodesObject)?.map((code) => ({ label: code, value: code }))}
                                                    />
                                                    <p className="invalid-feedback d-flex"> {validation.touched.country_code && validation.errors.country_code ? t(validation.errors.country_code) : null}</p>
                                                </div>
                                            </Col>
                                            <Col lg={12}>
                                                <div className="form-group">
                                                    <Input
                                                        name="phone_number"
                                                        className="form-control"
                                                        placeholder={t("Mobile number")}
                                                        type="text"
                                                        autoComplete="new-password"
                                                        onChange={validation.handleChange}
                                                        disabled={validation.values.country_code ? false : true}
                                                        onBlur={(e) => {
                                                            validation.handleBlur(e)
                                                            if (e.target.value) {
                                                                checkPhone(e.target.value, validation.values.country_code)
                                                            }
                                                        }}
                                                        value={validation.values.phone_number || ""}
                                                        invalid={
                                                            validation.touched.phone_number && validation.errors.phone_number ? true : false
                                                        }
                                                    />

                                                    {validation.touched.phone_number && validation.errors.phone_number ? (
                                                        <FormFeedback type="invalid">{validation.errors.phone_number}</FormFeedback>
                                                    ) : null}
                                                </div>
                                            </Col>
                                            <Col lg="12">
                                                <div className="form-group">
                                                    <Input
                                                        name="password"
                                                        value={validation.values.password || ""}
                                                        type="password"
                                                        className="form-control"
                                                        placeholder={t("Enter Password")}
                                                        onChange={validation.handleChange}
                                                        onBlur={validation.handleBlur}
                                                        autoComplete="new-password"
                                                        invalid={
                                                            validation.touched.password && validation.errors.password ? true : false
                                                        }
                                                    />
                                                    {validation.touched.password && validation.errors.password ? (
                                                        <FormFeedback type="invalid">{validation.errors.password}</FormFeedback>
                                                    ) : null}
                                                </div>
                                            </Col>
                                            <Col lg="12">
                                                <button className="btn submit" type="submit" >
                                                    {t("Signup")}
                                                </button>
                                                {/* <p className="or"><span>or</span></p>
                                                <ul className="social_login list-unstyled">
                                                    <li>
                                                        <button className="btn facebook">
                                                            <img src={facebook} className="img-fluid btn_icon" alt="icon" />
                                                            facebook
                                                        </button>
                                                    </li>
                                                    <li>
                                                        <button className="btn google">
                                                            <img src={google} className="img-fluid btn_icon" alt="icon" />
                                                            google
                                                        </button>
                                                    </li>
                                                </ul> */}
                                            </Col>

                                        </div>


                                    </div>
                                </Form>

                            </Col>
                        </Row>


                    </Container>
                </section>
            </ParticlesAuth>
            <Modal id="showModal" isOpen={OTBPopup} toggle={showOTBPopup} modalClassName='donate_modal' centered size="m">
                <ModalHeader toggle={showOTBPopup}>
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <i className="fa fa-times" aria-hidden="true"></i>
                    </button>
                </ModalHeader>

                <Form
                    className="tablelist-form"
                    onSubmit={(e) => {
                        e.preventDefault();
                        validationOTB.handleSubmit();
                        return false;
                    }}
                >
                    <ModalBody>
                        <div className="text ">
                            <div className="proForm">
                                <h4 className="title mb-3">{t("Phone Number Verification")}</h4>
                                <div className="form-group">
                                    <label className="form-label">{t("Please Enter OTB number")}</label>
                                    <Input
                                        name="otb"
                                        className="form-control"
                                        placeholder={t("OTB")}
                                        type="text"
                                        onChange={validationOTB.handleChange}
                                        onBlur={validationOTB.handleBlur}
                                        value={validationOTB.values.otb || ""}
                                        invalid={
                                            validationOTB.touched.otb && validationOTB.errors.otb ? true : false
                                        }
                                    />

                                    {validationOTB.touched.otb && validationOTB.errors.otb ? (
                                        <FormFeedback type="invalid">{validationOTB.errors.otb}</FormFeedback>
                                    ) : null}
                                </div>
                                <div className="form-group">
                                    <button className='shared'>
                                        {loading ? <Spinner size="sm" className='me-2'> Loading... </Spinner> : null}
                                        <span>{t("Send")}</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </ModalBody>
                </Form>
            </Modal>
        </>
    );
};

export default Register;
