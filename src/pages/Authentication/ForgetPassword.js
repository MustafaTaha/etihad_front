import PropTypes from "prop-types";
import React from "react";
import { Row, Col, Modal, ModalBody, ModalHeader, Container, FormFeedback, Input, Label, Form, Spinner } from "reactstrap";

//redux
import { useSelector, useDispatch } from "react-redux";

import { Link, useNavigate } from "react-router-dom";
import withRouter from "../../Components/Common/withRouter";

// Formik Validation
import * as Yup from "yup";
import { useFormik } from "formik";

// action
import { checkOTB_FP, checkPhoneExist_FP, userForgetPassword } from "../../slices/thunks";
import Select from "react-select";
import _ from "lodash";
import { useState } from "react";
import { useCallback } from "react";
import { toast } from 'react-toastify';

//import images
import login_imag from '../../styles/assets/images/login_imag.png';
import logo from '../../styles/assets/images/Logo.svg';
import pattern_top from '../../styles/assets/images/pattern_top.svg';
import pattern_bottom from '../../styles/assets/images/pattern_bottom.svg';
import facebook from '../../styles/assets/images/faccebook.svg';
import google from '../../styles/assets/images/google.svg';
import ParticlesAuth from "../AuthenticationInner/ParticlesAuth";
import { useTranslation } from "react-i18next";
import { useEffect } from "react";
import { phoneExist_FP, setIsSuccess_FP } from "../../slices/auth/forgetpwd/reducer";
const countryCodes = require('country-codes-list');
const myCountryCodesObject = countryCodes.customList('countryCode', '+{countryCallingCode}');
const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/

const ForgetPasswordPage = props => {
  const { t } = useTranslation();

  const dispatch = useDispatch();
  const history = useNavigate();

  const [registerObj, setRegisterObj] = useState({});
 
  const validation = useFormik({
    // enableReinitialize : use this flag when initial values needs to be changed
    enableReinitialize: true,
    validateOnChange: true,
    initialValues: {
      country_code: '',
      phone_number: ''
    },
    validationSchema: Yup.object({
      phone_number: Yup.string().required(t("Please Enter Your phone number")).matches(phoneRegExp, t('Phone number is not valid')),
      country_code: Yup.string().required(t("Please Select Country Code")),
    }),

    onSubmit: (values) => {
      setRegisterObj(values);
      showOTBPopup();
      let obj = {
        screen: "change_password",
        mobile_number: registerObj.phone_number
      }
      dispatch(checkOTB_FP(obj))
    }
  });

  const checkPhone = (phone_number, country_code) => {
    dispatch(checkPhoneExist_FP(phone_number, country_code))
  }
  const validationOTB = useFormik({
    // enableReinitialize : use this flag when initial values needs to be changed
    enableReinitialize: true,

    initialValues: {
      otb: '',
    },
    validationSchema: Yup.object({
      otb: Yup.number().typeError(t("otb must be a number")).required(t("Please Enter OTB number"))
    }),
    onSubmit: (values) => {
      values.otb == OTB_number ? dispatch(userForgetPassword(registerObj)) : toast.error("OTB is invaild")
    }
  });

  /*popup for enter OTB*/
  const [OTBPopup, setOTBPopup] = useState(false);

  const showOTBPopup = useCallback((item) => {
    if (OTBPopup) {
      setOTBPopup(false);
      validationOTB.resetForm();
    } else {
      setOTBPopup(true);
    }
  }, [OTBPopup]);

  const { OTB_number, error, success, message, loading, isPhoneExist } = useSelector(state => ({
    OTB_number: state.ForgetPassword.otb,
    success: state.ForgetPassword.success,
    error: state.ForgetPassword.error,
    loading: state.ForgetPassword.loading,
    message: state.ForgetPassword.response?.data?.message,
    isPhoneExist: state.ForgetPassword.phoneExist
  }));



  useEffect(() => {
    if (!isPhoneExist) {
      toast.error(t("Failed! User not exists with the provided phone number"))
      validation.setFieldError("phone_number", t("User not exists with the provided phone number"))
      validation.setFieldValue("phone_number", "")
      dispatch(phoneExist_FP(true))
    }
  }, [isPhoneExist])


  useEffect(() => {
    if (success) {
      toast.success(t("phone Verfied successfully"))
      setTimeout(() => history("/change-password"), 500);
      dispatch(setIsSuccess_FP(false))
    }

    // setTimeout(() => {
    //   dispatch(resetRegisterFlag());
    // }, 3000);

  }, [success]);

  // const validation = useFormik({
  //   // enableReinitialize : use this flag when initial values needs to be changed
  //   enableReinitialize: true,

  //   initialValues: {
  //     email: '',
  //   },
  //   validationSchema: Yup.object({
  //     email: Yup.string().required(t("Please Enter Your Email")),
  //   }),
  //   onSubmit: (values) => {
  //     dispatch(userForgetPassword(values, props.history));
  //   }
  // });

  const { forgetError, forgetSuccessMsg } = useSelector(state => ({
    forgetError: state.ForgetPassword.forgetError,
    forgetSuccessMsg: state.ForgetPassword.forgetSuccessMsg,
  }));

  return (
    <>

      <ParticlesAuth>
        <section className="login_page">
          <img src={pattern_top} className="img-fluid pattern top" alt="pattern" />
          <img src={pattern_bottom} className="img-fluid pattern bottom" alt="pattern" />
          <Container>
            <div className="logo">
              <Link to="/" href="index.html">
                <img src={logo} className="img-fluid" alt="logo" />
              </Link>
            </div>
            <Row>
              <Col lg="8">
                <div className="img">
                  <img src={login_imag} className="img-fluid" alt="image" />
                </div>
              </Col>
              <Col lg="4">
                <Form className="proForm"
                  onSubmit={(e) => {
                    e.preventDefault();
                    validation.handleSubmit();
                    return false;
                  }}
                  action="#">
                  <div className="text">
                    <div className="row">
                      <div className="col-12">
                        <h4 className="title">{t("Reset Password")}</h4>
                        <p className="desc">
                          {t("With your effort, you can make a difference in the lives of those in need. Join us today to help create a better world for all!")}
                        </p>
                        <p className="havAcc">{t("Don’t have account yet")}? <Link to="/register" className='link'>{t("Signup")}</Link>  </p>
                      </div>
                      <Col lg={12}>
                        <div className="form-group select">
                          <Select
                            placeholder={t("Country Code")}
                            value={_.values(myCountryCodesObject)?.map((country_code) => ({ label: country_code, value: country_code }))?.find((country_code) => country_code.value === validation.values.country_code) || ""}
                            onChange={(e) => {
                              validation.setFieldValue("country_code", e?.value);
                            }}
                            options={_.values(myCountryCodesObject)?.map((code) => ({ label: code, value: code }))}
                          />
                          <p className="invalid-feedback d-flex"> {validation.touched.country_code && validation.errors.country_code ? t(validation.errors.country_code) : null}</p>
                        </div>
                      </Col>
                      <Col lg={12}>
                        <div className="form-group">
                          <Input
                            name="phone_number"
                            className="form-control"
                            placeholder={t("Mobile number")}
                            type="text"
                            autoComplete="new-password"
                            onChange={validation.handleChange}
                            disabled={validation.values.country_code ? false : true}
                            onBlur={(e) => {
                              validation.handleBlur(e)
                              if (e.target.value) {
                                checkPhone(e.target.value, validation.values.country_code)
                              }
                            }}
                            value={validation.values.phone_number || ""}
                            invalid={
                              validation.touched.phone_number && validation.errors.phone_number ? true : false
                            }
                          />

                          {validation.touched.phone_number && validation.errors.phone_number ? (
                            <FormFeedback type="invalid">{validation.errors.phone_number}</FormFeedback>
                          ) : null}
                        </div>
                      </Col>
                      <Col lg="12">
                        <button className="btn submit">
                          {t("Send")}
                        </button>
                      </Col>

                    </div>


                  </div>
                </Form>

              </Col>
            </Row>


          </Container>
        </section>
      </ParticlesAuth>
      <Modal id="showModal" isOpen={OTBPopup} toggle={showOTBPopup} modalClassName='donate_modal' centered size="m">
        <ModalHeader toggle={showOTBPopup}>
          <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close">
            <i className="fa fa-times" aria-hidden="true"></i>
          </button>
        </ModalHeader>

        <Form
          className="tablelist-form"
          onSubmit={(e) => {
            e.preventDefault();
            validationOTB.handleSubmit();
            return false;
          }}
        >
          <ModalBody>
            <div className="text ">
              <div className="proForm">
                <h4 className="title mb-3">{t("Phone Number Verification")}</h4>
                <div className="form-group">
                  <label className="form-label">{t("Please Enter OTB number")}</label>
                  <Input
                    name="otb"
                    className="form-control"
                    placeholder={t("OTB")}
                    type="text"
                    onChange={validationOTB.handleChange}
                    onBlur={validationOTB.handleBlur}
                    value={validationOTB.values.otb || ""}
                    invalid={
                      validationOTB.touched.otb && validationOTB.errors.otb ? true : false
                    }
                  />

                  {validationOTB.touched.otb && validationOTB.errors.otb ? (
                    <FormFeedback type="invalid">{validationOTB.errors.otb}</FormFeedback>
                  ) : null}
                </div>
                <div className="form-group">
                  <button className='shared'>
                    {loading ? <Spinner size="sm" className='me-2'> Loading... </Spinner> : null}
                    <span>{t("Send")}</span>
                  </button>
                </div>
              </div>
            </div>
          </ModalBody>
        </Form>
      </Modal>
    </ >
  );
};

ForgetPasswordPage.propTypes = {
  history: PropTypes.object,
};

export default withRouter(ForgetPasswordPage);
