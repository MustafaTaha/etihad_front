import React, { useEffect, useState, useCallback } from 'react';
import {  Col, Container, Input,  Row,  Form, FormFeedback,  Spinner } from 'reactstrap';
import ParticlesAuth from "../AuthenticationInner/ParticlesAuth";


//redux
import { useSelector, useDispatch } from "react-redux";

import { Link, redirect, useNavigate } from "react-router-dom";
import withRouter from "../../Components/Common/withRouter";
// Formik validation
import * as Yup from "yup";
import { useFormik } from "formik";

// actions
import { loginUser, socialLogin, resetLoginFlag, } from "../../slices/thunks";
import Select from "react-select";
import _ from "lodash";
//import images
import login_imag from '../../styles/assets/images/login_imag.png';
import logo from '../../styles/assets/images/Logo.svg';
import pattern_top from '../../styles/assets/images/pattern_top.svg';
import pattern_bottom from '../../styles/assets/images/pattern_bottom.svg';
import facebook from '../../styles/assets/images/faccebook.svg';
import google from '../../styles/assets/images/google.svg';
import { toast } from 'react-toastify';
import { useTranslation } from 'react-i18next';
import { LoginSocialGoogle } from 'reactjs-social-login';

const countryCodes = require('country-codes-list');
const myCountryCodesObject = countryCodes.customList('countryCode', '+{countryCallingCode}');
const phoneRegExp = /^((\\+[1-9]{1,4}[ \\-]*)|(\\([0-9]{2,3}\\)[ \\-]*)|([0-9]{2,4})[ \\-]*)*?[0-9]{3,4}?[ \\-]*[0-9]{3,4}?$/


const PhoneLogin = (props) => {
    const { t } = useTranslation();
    const navigate = useNavigate();
    const dispatch = useDispatch();
    const { user, error, loading, errorMsg } = useSelector(state => ({
        user: state.Account.user,
        error: state.Login.error,
        loading: state.Login.loading,
        errorMsg: state.Login.errorMsg,
    }));

    const [userLogin, setUserLogin] = useState([]);
    const [showPassword, setShowPassword] = useState(false);

    useEffect(() => {
        error ? toast.error(error) : null
    }, [error])

   
    const validation = useFormik({
        // enableReinitialize : use this flag when initial values needs to be changed
        enableReinitialize: true,
        validateOnChange: true,
        initialValues: {
            country_code: '',
            phone_number: '',
            password:''
        },
        validationSchema: Yup.object({
            phone_number: Yup.string().required(t("Please Enter Your phone number")).matches(phoneRegExp, t('Phone number is not valid')),
            country_code: Yup.string().required(t("Please Select Country Code")),
            password: Yup.string().required(t("Please Enter Your Password")),
        }),

        onSubmit: (values) => {
            let obj = {
                ...values,
                login_type: "phone_number",
                social_media_name : "own"
            }
            dispatch(loginUser(obj, props.router.navigate));
        }
    });

    const signIn = type => {
        dispatch(socialLogin(type, props.router.navigate));
    };

    //handleTwitterLoginResponse
    // const twitterResponse = e => {}

    //for facebook and google authentication
    const socialResponse = type => {
        signIn(type);
    };


    useEffect(() => {
        if (errorMsg) {
            setTimeout(() => {
                dispatch(resetLoginFlag());
            }, 3000);
        }
    }, [dispatch, errorMsg]);

    const [provider, setProvider] = useState('')
    const [profile, setProfile] = useState(null)

    const onLoginStart = useCallback(() => {

    }, [])
    return (
        <ParticlesAuth>
            <section className="login_page">
                <img src={pattern_top} className="img-fluid pattern top" alt="pattern" />
                <img src={pattern_bottom} className="img-fluid pattern bottom" alt="pattern" />
                <Container>
                    <div className="logo">
                        <Link to="/" href="index.html">
                            <img src={logo} className="img-fluid" alt="logo" />
                        </Link>
                    </div>
                    <Row>
                        <Col lg="8">
                            <div className="img">
                                <img src={login_imag} className="img-fluid" alt="image" />
                            </div>
                        </Col>
                        <Col lg="4">
                            <Form className="proForm"
                                onSubmit={(e) => {
                                    e.preventDefault();
                                    validation.handleSubmit();
                                    return false;
                                }}
                                action="#">
                                <div className="text">
                                    <div className="row">
                                        <div className="col-12">
                                            <h4 className="title">{t("Login")}</h4>
                                            <p className="desc">
                                                {t("With your effort, you can make a difference in the lives of those in need. Join us today to help create a better world for all!")}
                                            </p>
                                            <p className="havAcc">{t("Don’t have account yet")}? <Link to="/register" className='link'>{t("Signup")}</Link>  </p>
                                        </div>
                                        <Col lg={12}>
                                            <div className="form-group select">
                                                <Select
                                                    placeholder={t("Country Code")}
                                                    value={_.values(myCountryCodesObject)?.map((country_code) => ({ label: country_code, value: country_code }))?.find((country_code) => country_code.value === validation.values.country_code) || ""}
                                                    onChange={(e) => {
                                                        validation.setFieldValue("country_code", e?.value);
                                                    }}
                                                    options={_.values(myCountryCodesObject)?.map((code) => ({ label: code, value: code }))}
                                                />
                                                <p className="invalid-feedback d-flex"> {validation.touched.country_code && validation.errors.country_code ? t(validation.errors.country_code) : null}</p>
                                            </div>
                                        </Col>
                                        <Col lg={12}>
                                            <div className="form-group">
                                                <Input
                                                    name="phone_number"
                                                    className="form-control"
                                                    placeholder={t("Mobile number")}
                                                    type="text"
                                                    autoComplete="new-password"
                                                    onChange={validation.handleChange}
                                                    // disabled={validation.values.country_code ? false : true}
                                                    onBlur={validation.handleBlur}
                                                    value={validation.values.phone_number || ""}
                                                    invalid={
                                                        validation.touched.phone_number && validation.errors.phone_number ? true : false
                                                    }
                                                />

                                                {validation.touched.phone_number && validation.errors.phone_number ? (
                                                    <FormFeedback type="invalid">{validation.errors.phone_number}</FormFeedback>
                                                ) : null}
                                            </div>
                                        </Col>
                                        <Col lg={12}>
                                            <div className="form-group">
                                                <Input
                                                    name="password"
                                                    value={validation.values.password || ""}
                                                    type={showPassword ? "text" : "password"}
                                                    className="form-control"
                                                    placeholder={t("Enter Password")}
                                                    onChange={validation.handleChange}
                                                    onBlur={validation.handleBlur}
                                                    invalid={
                                                        validation.touched.password && validation.errors.password ? true : false
                                                    }
                                                />
                                                {validation.touched.password && validation.errors.password ? (
                                                    <FormFeedback type="invalid">{validation.errors.password}</FormFeedback>
                                                ) : null}
                                            </div>
                                        </Col>
                                        <div className="col-12">
                                            <div className='d-flex justify-content-between align-items-center flex-wrap'>
                                                <p className="fogetPass">{t("Login with email")}? <Link to="/login" className='link'>{t("click here")}</Link>  </p>
                                                <p className="fogetPass">{t("forget password")}?  <Link to="/forgot-password" className="link"> {t("click here")}</Link>  </p>
                                            </div>
                                        </div>
                                        <div className="col-12">
                                            <button className="btn submit"  >
                                                {loading ? <Spinner size="sm" className='me-2'> Loading... </Spinner> : null}
                                                {t("Login")}
                                            </button>
                                            {/* <LoginSocialGoogle
                                                isOnlyGetToken={false}
                                                client_id={'586204175153-m4suiis88ijkatnstdfhe268af3esqd2.apps.googleusercontent.com' || ''}
                                                onLoginStart={onLoginStart}
                                                onResolve={({ provider, data }) => {
                                                    setProvider(provider)
                                                    setProfile(data)

                                                    navigate('/')

                                                }}
                                                onReject={(err) => {
                                                    console.log(err)
                                                }}
                                                redirect_uri={'/'}
                                            >
                                                login with google
                                            </LoginSocialGoogle> */}
                                            {/* <p className="or"><span>or</span></p>
                                            <ul className="social_login list-unstyled">
                                                <li>
                                                    <Link
                                                        to="#"
                                                        className="btn facebook"
                                                        onClick={e => {
                                                            e.preventDefault();
                                                            socialResponse("facebook");
                                                        }}
                                                    >
                                                        <img src={facebook} className="img-fluid btn_icon" alt="icon" />
                                                        facebook
                                                    </Link>
                                                </li>
                                                <li> 
                                                    <Link
                                                        to="#"
                                                        className="btn google"
                                                        onClick={e => {
                                                            e.preventDefault();
                                                            socialResponse("google");
                                                        }}
                                                    >
                                                        <img src={google} className="img-fluid btn_icon" alt="icon" />
                                                        google
                                                    </Link>
                                                </li>
                                            </ul> */}
                                        </div>

                                    </div>


                                </div>
                            </Form>

                        </Col>
                    </Row>


                </Container>
            </section>
        </ParticlesAuth>
    );
};

export default withRouter(PhoneLogin);