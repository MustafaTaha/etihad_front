import React from 'react';
import withRouter from '../../Components/Common/withRouter';

const ParticlesAuth = ({ children }) => {
    return (
        <>
            {/* pass the children */}
            {children}
        </>
    );
};

export default withRouter(ParticlesAuth);