import React, { useEffect } from 'react';
import moment from 'moment';
import donaite from '../../styles/assets/images/donaite.png';
import Header from '../../Components/Common/Header';
import Footer from '../../Components/Common/Footer';

//redux
import { useSelector, useDispatch } from 'react-redux';


//import action
import {
    getNewsSingle as onGetNewsDetails,
} from "../../slices/thunks";
import i18n from '../../i18n';
import { useTranslation } from 'react-i18next';
import { Link, useParams } from 'react-router-dom';



const NewsSingle = (props) => {
    const { t } = useTranslation();

    const dispatch = useDispatch();
    let { id } = useParams();
    console.log(id);
    const { newsDetails } = useSelector((state) => ({
        newsDetails: state.News.newsDetails,
    }));

    useEffect(() => {
        dispatch(onGetNewsDetails(id));
    }, []);


    return (
        <>
            <Header />
            {
                console.log(newsDetails, "mustafa")
            }
            <main>
                <section className="bread_crumb">
                    <div className="container">
                        <h3 className="title">{t("News details")}</h3>
                    </div>
                </section>

                <section className="our_projects single medi_center p-t-b">
                    <div className="container">
                        <div className="sec_title" data-aos="fade-up">
                            <h3 className="title mb-4">
                                {i18n.language === "ar" ? newsDetails?.title_ar : newsDetails?.title_en}
                            </h3>
                            <div className='mb-4'>
                                <img src={newsDetails?.thumbnail_url} className='img-fluid w-100' alt={i18n.language === "ar" ? newsDetails?.title_ar : newsDetails?.title_en} />
                            </div>
                            <div className="desc">
                                <div dangerouslySetInnerHTML={{ __html: i18n.language === "ar" ? newsDetails?.description_ar : newsDetails?.description_en }} />
                                {/* {i18n.language === "ar" ? item?.description_ar : item?.description_en} */}
                            </div>
                        </div>
                    </div>
                </section>

            </main>
            <Footer />
        </>
    );
}

export default NewsSingle;