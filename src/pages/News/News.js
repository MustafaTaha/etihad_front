import React, { useEffect } from 'react';
import moment from 'moment';
import donaite from '../../styles/assets/images/donaite.png';
import Header from '../../Components/Common/Header';
import Footer from '../../Components/Common/Footer';

//redux
import { useSelector, useDispatch } from 'react-redux';


//import action
import {
    getNewsList as onGetNewsList,
} from "../../slices/thunks";
import i18n from '../../i18n';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';



const News = () => {
    const { t } = useTranslation();

    const dispatch = useDispatch();

    const { newsLists } = useSelector((state) => ({
        newsLists: state.News.newsLists || [],
    }));

    useEffect(() => {
        dispatch(onGetNewsList());
    }, []);


    return (
        <>
            <Header />
            <main>
                <section className="bread_crumb">
                    <div className="container">
                        <h3 className="title">{t("Latest News")}</h3>
                    </div>
                </section>

                <section className="our_projects medi_center p-t-b">
                    <div className="container">
                        <div className="row">
                            <div className="col-lg-9">
                                <div className="sec_title" data-aos="fade-up">
                                    <h4 className="sub_title">{t("News")}</h4>
                                    <h3 className="title">
                                        {t("Sharing what we do would encourage others Too!")}
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div className="row">
                            {(newsLists || []).map((item) => (
                                <div className="col-lg-3 col-md-6" key={item.id}>
                                    <Link to={`/news/${item.id}`} className="donaite-card" data-aos="fade-up">
                                        <div className="donaite-image">
                                            <img src={item.thumbnail_url ? item.thumbnail_url : donaite} alt="donaite" className="img-fluid" loading={'lazy'} />
                                        </div>
                                        <div className="text">
                                            <div className="details">
                                                <p className="num">{moment(item.created_on).format('LL')} </p>
                                            </div>
                                            <p className="name line2"> {i18n.language === "ar" ? item?.title_ar : item?.title_en}</p>
                                        </div>
                                    </Link>
                                </div>
                            ))}
                        </div>
                    </div>
                </section>

            </main>
            <Footer />
        </>
    );
}

export default News;