import React, { useCallback, useEffect, useState } from 'react';
import moment from 'moment';

import { Col, Input, Modal, ModalBody, ModalHeader, Form, Spinner, FormFeedback } from 'reactstrap';
import { Navigation, Pagination, Scrollbar, A11y, Autoplay } from 'swiper';
import { Swiper, SwiperSlide } from 'swiper/react';
import { Player } from 'video-react';

import sliderBG from '../../styles/assets/images/siderBG.png';
import business_type from '../../styles/assets/images/business_type.png';
import business_type2 from '../../styles/assets/images/business_type2.png';
import business_type3 from '../../styles/assets/images/business_type3.png';
import business_type4 from '../../styles/assets/images/business_type4.png';
import donaite from '../../styles/assets/images/donaite.png';
import search from '../../styles/assets/images/search_w.svg';
import pattern from '../../styles/assets/images/pattern.svg';
import slide1 from '../../styles/assets/images/el-etihad/slide1.png';
import slide2 from '../../styles/assets/images/el-etihad/slide2.png';
import slide3 from '../../styles/assets/images/el-etihad/slide3.png';
import slide4 from '../../styles/assets/images/el-etihad/slide4.png';
import slide5 from '../../styles/assets/images/el-etihad/slide5.png';
import slide6 from '../../styles/assets/images/el-etihad/slide6.png';
import slide7 from '../../styles/assets/images/el-etihad/slide7.png';
import slide8 from '../../styles/assets/images/el-etihad/slide8.png';
import slide9 from '../../styles/assets/images/el-etihad/slide9.png';
import slide10 from '../../styles/assets/images/el-etihad/slide10.png';
import slide11 from '../../styles/assets/images/el-etihad/slide11.png';
import slide12 from '../../styles/assets/images/el-etihad/slide12.png';
import slide13 from '../../styles/assets/images/el-etihad/slide13.png';
import slide14 from '../../styles/assets/images/el-etihad/slide14.png';
import slide15 from '../../styles/assets/images/el-etihad/slide15.png';
import slide16 from '../../styles/assets/images/el-etihad/slide16.png';

import video_splash from '../../styles/assets/images/video_splash.png';
import video_RUL from '../../styles/assets/images/video.mp4';

import Header from '../../Components/Common/Header';
import Footer from '../../Components/Common/Footer';

//redux
import { useSelector, useDispatch } from 'react-redux';


//import action
import {
    getSmscodes as onGetSmscodes,
    getProjectList as onGetProjectList,
    getNewsList as onGetNewsList,
    getMain_sliderList as onGetMain_sliderList,
    userPayment,
} from "../../slices/thunks";
import i18n from '../../i18n';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
// Formik validation
import * as Yup from "yup";
import { useFormik } from "formik";
import IsLoading from "../../Components/Common/IsLoading";

const Home = () => {
    const { t } = useTranslation();

    const dispatch = useDispatch();

    const { projectLists, user_info, project_loading, newsLists, main_sliderList, smsCodeList, isSMSLoading } = useSelector((state) => ({
        main_sliderList: state.Main_slider.main_sliderList,
        smsCodeList: state.Projects.smsCodeList,
        projectLists: state.Projects.projectLists,
        newsLists: state.News.newsLists,
        user_info: state.Login.user.user,
        project_loading: state.Payment.loading,
        isSMSLoading: state.Projects.isSMSLoading,
    }));

    /*get single project id*/
    const [project_id, setProject_id] = useState(null);

    /*get single project*/
    const [project, setProject] = useState({});


    useEffect(() => {
        dispatch(onGetProjectList());
        dispatch(onGetNewsList());
        dispatch(onGetMain_sliderList());
    }, []);


    //search for project

    const [filteredProjects, setFilteredProjects] = useState(false);
    const [searchQuery, setSearchQuery] = useState(false);

    useEffect(() => {
        if (searchQuery) {
            let filtered = [];
            main_sliderList?.forEach((project) => {
                if (project?.title_en?.includes(searchQuery) || project?.title_ar?.includes(searchQuery)) {
                    filtered.push(project);
                }
            });
            setFilteredProjects(filtered);
        } else {
            setFilteredProjects(false);
        }
    }, [searchQuery]);


    const [modal, setModal] = useState(false);
    const toggle = useCallback(() => {
        if (modal) {
            setModal(false);
            // setLead(false);
            // validation.resetForm();
        } else {
            setModal(true);
        }
    }, [modal]);

    /*popup for enter amount*/
    const [amountPopup, setAmountPopup] = useState(false);

    const showAmountPopup = useCallback((item) => {
        if (amountPopup) {
            setAmountPopup(false);
            // setLead(false);
            validation.resetForm();
        } else {
            setAmountPopup(true);
        }
    }, [amountPopup]);
    //handle user payment
    const handleUserPayment = (data) => {
        setProject_id(data?.id)
        let obj = {
            user_id: user_info ? user_info.id : 0,
            donation_id: data.id,
            donation_type: data.donation_type,
            amount: data.amount,
            device: "web",
            item_json: JSON.stringify(data),
            other_comments: ""
        }
        dispatch(userPayment(obj));

    }

    const validation = useFormik({
        // enableReinitialize : use this flag when initial values needs to be changed
        enableReinitialize: true,

        initialValues: {
            amount: '',
        },
        validationSchema: Yup.object({
            amount: Yup.number().typeError(t("Amount must be a number")).min(1, "can not be 0").required(t("Please Enter amount"))
        }),
        onSubmit: (values) => {

            let obj = {
                user_id: user_info ? user_info.id : 0,
                donation_id: project.id,
                donation_type: project.donation_type,
                amount: values.amount,
                device: "web",
                item_json: JSON.stringify(project),
                other_comments: ""
            }
            dispatch(userPayment(obj));
            if (!project_loading) {
                setTimeout(() => {
                    setAmountPopup(false);
                    validation.resetForm();
                }, 2000);
            }
        }
    });

    /*popup for SMS Codes*/
    const [smsPopup, setSmsPopup] = useState(false);

    const showSmsPopup = useCallback((item) => {
        if (smsPopup) {
            setSmsPopup(false);
        } else {
            dispatch(onGetSmscodes({ id: item.project_id, donation_type: item.project_type }));
            setSmsPopup(true);
        }
    }, [smsPopup]);
    return (
        <>

            <Header />
            <main>
                <section className="main-slider">
                    <div className="container">
                        <Swiper
                            modules={[Autoplay, Navigation, Pagination, Scrollbar, A11y]}
                            spaceBetween={50}
                            slidesPerView={1}
                            autoplay={{
                                "delay": 2500,
                            }}
                            navigation
                            // pagination={{ clickable: true }}
                            scrollbar={{ draggable: true }}

                            breakpoints={{
                                640: {
                                    slidesPerView: 1,
                                    spaceBetween: 20,
                                },
                                768: {
                                    slidesPerView: 1,
                                    spaceBetween: 20,
                                },
                                1024: {
                                    slidesPerView: 1,
                                    spaceBetween: 20,
                                },
                                1200: {
                                    slidesPerView: 1,
                                    spaceBetween: 20,
                                }
                            }}
                        >
                            {(main_sliderList || []).map((item) => (
                                <SwiperSlide style={{ backgroundImage: `url( ${item?.thumbnail_url ? item.thumbnail_url : sliderBG})` }} key={item.id}>
                                    <div className="row">
                                        <div className="col-lg-6">
                                            <div className="slider-content">
                                                <h1 className="title">{i18n.language === "ar" ? item?.title_ar : item?.title_en}</h1>
                                                <div className="desc">
                                                    <div dangerouslySetInnerHTML={{ __html: i18n.language === "ar" ? item?.description_ar : item?.description_en }} />
                                                    {/* {i18n.language === "ar" ? item?.description_ar : item?.description_en} */}
                                                </div>
                                                <ul className="list-unstyled m_actions">
                                                    <li>
                                                        {/* <button type='button' className="btn primaryBtn shared" onClick={() => handleUserPayment(item)}>
                                                            {project_loading && project_id == item.id ? <Spinner size="sm" className='me-2'> Loading... </Spinner> : null}
                                                            <span>{t("Donate Now")}</span>
                                                        </button> */}
                                                        <button type='button' className="btn primaryBtn shared" onClick={() => { showAmountPopup(); setProject(item) }}>
                                                            <span>{t("Donate Now")}</span>
                                                        </button>
                                                    </li>
                                                    <li>
                                                        <a href="#" className="btn blackBtn shared">
                                                            <span>NFT </span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a className="btn secondaryBtn shared" onClick={() => { showSmsPopup(item); }} >
                                                            <span>SMS </span>
                                                        </a>
                                                    </li>
                                                </ul>

                                            </div>
                                        </div>
                                    </div>
                                </SwiperSlide>
                            ))}

                        </Swiper>
                    </div>
                </section>

                <section className="business_area p-t-b">
                    <div className="container">
                        <div className="sec_title">
                            <h3 className="title" data-aos="fade-up">{t("Our business areas")} :</h3>
                        </div>
                        <div className="business_types">
                            <div className="business_type odd" data-aos="fade-up">
                                <div className="img">
                                    <img src={business_type} className="img-fluid" alt="business_type" />
                                </div>
                                <div className="text">
                                    <h5 className="title">{t("charity projects")}</h5>
                                    <p className="desc"> {t("business_type")}</p>
                                    <span className="num">2,184</span>
                                </div>
                            </div>
                            <div className="business_type" data-aos="fade-up">
                                <div className="img">
                                    <img src={business_type2} className="img-fluid" alt="business_type" />
                                </div>
                                <div className="text">
                                    <h5 className="title">{t("Charity education")}</h5>
                                    <p className="desc">{t("business_type2")} </p>
                                    <span className="num">42,738</span>
                                </div>
                            </div>
                            <div className="business_type odd" data-aos="fade-up">
                                <div className="img">
                                    <img src={business_type3} className="img-fluid" alt="business_type" />
                                </div>
                                <div className="text">
                                    <h5 className="title">{t("Sponsorships and orphan care")}</h5>
                                    <p className="desc">{t("business_type3")}</p>
                                    <span className="num">4,500</span>
                                </div>
                            </div>
                            <div className="business_type" data-aos="fade-up">
                                <div className="img">
                                    <img src={business_type4} className="img-fluid" alt="business_type" />
                                </div>
                                <div className="text">
                                    <h5 className="title">{t("humanitarian cases")}</h5>
                                    <p className="desc">{t("business_type4")}</p>
                                    <span className="num">2,045</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

                <section className="our_projects p-t-b" id='projects'>
                    <div className="container">
                        <div className="row align-items-center">
                            <div className="col-lg-9">
                                <div className="sec_title" data-aos="fade-up">
                                    <h3 className="title">{t("Our Projects")} :</h3>
                                </div>
                            </div>
                            <div className="col-lg-3">
                                <div className="proForm">
                                    <div className="form-group">
                                        <Input type="text" className="form-control search" value={searchQuery ? searchQuery : ""} onChange={(e) => setSearchQuery(e.target.value)} placeholder={t("search")} />
                                        <img src={search} className="img-fluid icon" alt="search" />
                                    </div>
                                </ div>
                            </div>
                        </div>

                        <div className="row">

                            {(filteredProjects || main_sliderList || []).map((item) => (
                                <Col xl={3} lg={4} md={6} key={item.id}>
                                    <div className="donaite-card" data-aos="fade-up">
                                        <a className="donaite-image">
                                            <img src={item.thumbnail_url ? item.thumbnail_url : donaite} alt="donaite" className="img-fluid" />
                                        </a>
                                        <div className="text">
                                            <div className="details">
                                                <p className="num">{item.amount}</p>
                                                <ul className="list-unstyled m_actions">
                                                    <li>
                                                        {/* handleUserPayment(item) */}
                                                        <button type='button' className="btn primaryBtn shared" onClick={() => { showAmountPopup(); setProject(item) }}>
                                                            <span>{t("Donate Now")}</span>
                                                        </button>
                                                    </li>
                                                    <li>
                                                        <a className="btn blackBtn shared">
                                                            <span>NFT </span>
                                                        </a>
                                                    </li>


                                                    <li>
                                                        <a className="btn secondaryBtn shared" onClick={() => { showSmsPopup(item); }} >
                                                            <span>SMS </span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                            <a className="name">{i18n.language === "ar" ? item?.title_ar : item?.title_en}</a>
                                        </div>
                                    </div>
                                </Col>
                            ))}
                        </div>
                    </div>
                </section>

                <section className="boards p-t-b">
                    <div className="container">
                        <div className="board_BG" data-aos="fade-up">
                            <Link to={"/word-of-board-firectors"} className="title">
                                {t("THE WORD OF THE BOARD OF DIRECTORS")}
                                <span className="b1"></span>
                                <span className="b2"></span>
                                <span className="b3"></span>
                                <span className="b4"></span>
                            </Link>
                        </div>
                    </div>
                </section>

                <section className="our_projects medi_center p-t-b">
                    <div className="container">
                        <div className="sec_title" data-aos="fade-up">
                            <h3 className="title"> {t("Media Center")} </h3>
                        </div>
                        <div className="row">
                            {(newsLists.slice(0, 4) || []).map((item) => (
                                <div className="col-lg-3 col-md-6" key={item.id}>
                                    <Link to={`/news/${item.id}`} className="donaite-card" data-aos="fade-up">
                                        <div className="donaite-image">
                                            <img src={item.thumbnail_url ? item.thumbnail_url : donaite} alt="donaite" className="img-fluid" />
                                        </div>
                                        <div className="text">
                                            <div className="details">
                                                <p className="num">{moment(item.created_on).format('LL')} </p>
                                            </div>
                                            <p className="name line2"> {i18n.language === "ar" ? item?.title_ar : item?.title_en}</p>
                                        </div>
                                    </Link>
                                </div>
                            ))}
                        </div>
                    </div>
                </section>

                <section className="pattern p-t-b">
                    <div className="container">
                        <div className="love" data-aos="fade-up">
                            <span>{t("Love")}</span>
                            <img src={pattern} className="img-fluid" alt="pattern" />
                            <span>{t("Found")}</span>
                        </div>
                    </div>
                </section>

                <section className="clinets p-t-b">
                    <div className="container">
                        <div className="sec_title" data-aos="fade-up">
                            <h3 className="title">{t("Success partners")} </h3>
                        </div>
                    </div>
                    <div className="swiper-container first" data-aos="fade-up">

                        <Swiper
                            modules={[Autoplay, Navigation, Pagination, A11y]}
                            spaceBetween={5}
                            slidesPerView={1}
                            speed={1000}
                            autoplay={{
                                "delay": 2500,
                            }}
                            loop={true}
                            // pagination={{ clickable: true }}
                            scrollbar={{ draggable: true }}

                            breakpoints={{
                                425: {
                                    slidesPerView: 2
                                },
                                576: {
                                    slidesPerView: 3

                                },
                                740: {
                                    slidesPerView: 3
                                },
                                960: {
                                    slidesPerView: 5
                                },
                                1280: {
                                    slidesPerView: 8
                                }
                            }}
                        >
                            <SwiperSlide>
                                <a href="#">
                                    <img src={slide1} alt='clinet' />
                                </a>
                            </SwiperSlide>
                            <SwiperSlide>
                                <a href="#">
                                    <img src={slide2} alt='clinet' />
                                </a>
                            </SwiperSlide>
                            <SwiperSlide>
                                <a href="#">
                                    <img src={slide3} alt='clinet' />
                                </a>
                            </SwiperSlide>
                            <SwiperSlide>
                                <a href="#">
                                    <img src={slide4} alt='clinet' />
                                </a>
                            </SwiperSlide>
                            <SwiperSlide>
                                <a href="#">
                                    <img src={slide5} alt='clinet' />
                                </a>
                            </SwiperSlide>
                            <SwiperSlide>
                                <a href="#">
                                    <img src={slide6} alt='clinet' />
                                </a>
                            </SwiperSlide>
                            <SwiperSlide>
                                <a href="#">
                                    <img src={slide7} alt='clinet' />
                                </a>
                            </SwiperSlide>
                            <SwiperSlide>
                                <a href="#">
                                    <img src={slide8} alt='clinet' />
                                </a>
                            </SwiperSlide>

                        </Swiper>
                    </div>

                    <div className="swiper-container second" data-aos="fade-up">

                        <Swiper
                            modules={[Autoplay, Navigation, Pagination, A11y]}
                            spaceBetween={5}
                            slidesPerView={1}
                            speed={1000}
                            autoplay={{
                                "delay": 2500,
                            }}
                            dir={i18n.language === "ar" ? "ltr" : "rtl"}
                            loop={true}
                            // pagination={{ clickable: true }}
                            scrollbar={{ draggable: true }}

                            breakpoints={{
                                425: {
                                    slidesPerView: 2
                                },
                                576: {
                                    slidesPerView: 3

                                },
                                740: {
                                    slidesPerView: 3
                                },
                                960: {
                                    slidesPerView: 5
                                },
                                1280: {
                                    slidesPerView: 8
                                }
                            }}
                        >
                            <SwiperSlide>
                                <a href="#">
                                    <img src={slide9} alt='clinet' />
                                </a>
                            </SwiperSlide>
                            <SwiperSlide>
                                <a href="#">
                                    <img src={slide10} alt='clinet' />
                                </a>
                            </SwiperSlide>
                            <SwiperSlide>
                                <a href="#">
                                    <img src={slide11} alt='clinet' />
                                </a>
                            </SwiperSlide>
                            <SwiperSlide>
                                <a href="#">
                                    <img src={slide12} alt='clinet' />
                                </a>
                            </SwiperSlide>
                            <SwiperSlide>
                                <a href="#">
                                    <img src={slide13} alt='clinet' />
                                </a>
                            </SwiperSlide>
                            <SwiperSlide>
                                <a href="#">
                                    <img src={slide14} alt='clinet' />
                                </a>
                            </SwiperSlide>
                            <SwiperSlide>
                                <a href="#">
                                    <img src={slide15} alt='clinet' />
                                </a>
                            </SwiperSlide>
                            <SwiperSlide>
                                <a href="#">
                                    <img src={slide16} alt='clinet' />
                                </a>
                            </SwiperSlide>

                        </Swiper>
                    </div>
                </section>

                <section className="video_js ">
                    <div className="container">
                        <div className="sec_title text-center" data-aos="fade-up">
                            <h3 className="title">{t("Making A Good Impact In The Others’ lives Is Our Ultimate Goal")}</h3>
                        </div>
                        <Player
                            playsInline
                            poster={video_splash}
                            src={video_RUL}
                        />

                    </div>
                </section>
                <section>
                    {/* <a href="sms:+18664504185?&body=Hi%2520there%252C%2520I%2527d%2520like%2520to%2520place%2520an%2520order%2520for...">Click here to text us!</a> */}

                </section>
            </main>
            <Footer />



            <Modal id="showModal" isOpen={modal} toggle={toggle} modalClassName='donate_modal' centered size="xl">
                <ModalHeader toggle={toggle}>
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <i className="fa fa-times" aria-hidden="true"></i>
                    </button>
                </ModalHeader>

                <Form
                    className="tablelist-form"
                    onSubmit={(e) => {
                        e.preventDefault();
                        // validation.handleSubmit();
                        return false;
                    }}
                >
                    <ModalBody>
                        <div className="row">
                            <div className="col-lg-7">
                                <div className="text">
                                    <h4 className="title">Invest in a Better Tomorrow: Donate to Our Cause.</h4>
                                    <p className="by">Donate by:</p>
                                    <ul className="nav nav-pills mb-3" id="pills-tab" role="tablist">
                                        <li className="nav-item" role="presentation">
                                            <button className="nav-link active" id="pills-home-tab" data-bs-toggle="pill"
                                                data-bs-target="#Bank-account" type="button" role="tab" aria-controls="Bank-account"
                                                aria-selected="true">
                                                <div className="form-check">
                                                    <input className="form-check-input" type="radio" name="donate" id="flexRadioDefault1" checked />
                                                    <div className="circle">
                                                    </div>
                                                    <label className="form-check-label" htmlFor="flexRadioDefault1">
                                                        Bank account
                                                    </label>
                                                </div>
                                            </button>
                                        </li>
                                        <li className="nav-item" role="presentation">
                                            <button className="nav-link" id="mobile-tab" data-bs-toggle="pill" data-bs-target="#mobile"
                                                type="button" role="tab" aria-controls="mobile" aria-selected="false">
                                                <div className="form-check">
                                                    <input className="form-check-input" type="radio" name="donate" id="flexRadioDefault2" />
                                                    <label className="form-check-label" htmlFor="flexRadioDefault2">
                                                        Mobile message
                                                    </label>
                                                </div>
                                            </button>
                                        </li>
                                        <li className="nav-item" role="presentation">
                                            <button className="nav-link" id="NFT-tab" data-bs-toggle="pill" data-bs-target="#NFT" type="button"
                                                role="tab" aria-controls="NFT" aria-selected="false">
                                                <div className="form-check">
                                                    <input className="form-check-input" type="radio" name="donate" id="flexRadioDefault3" />
                                                    <label className="form-check-label" htmlFor="flexRadioDefault3">
                                                        NFT
                                                    </label>
                                                </div>
                                            </button>
                                        </li>
                                    </ul>
                                    <div className="tab-content" id="pills-tabContent">
                                        <div className="tab-pane fade show active" id="Bank-account" role="tabpanel"
                                            aria-labelledby="Bank-account-tab">
                                            <form action="#" className="proForm">
                                                <div className="row">
                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <label htmlFor="card_number" className="form-label">Card Number</label>
                                                            <input type="text" className="form-control" id="card_number"
                                                                placeholder="1234  5678  9101  1121" />
                                                            <div className="valid-feedback"> Looks good! </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-6">
                                                        <div className="form-group">
                                                            <label htmlFor="Expiration_Date" className="form-label">Expiration Date</label>
                                                            <input type="text" className="form-control" id="Expiration_Date" placeholder="MM / YY" />
                                                            <div className="valid-feedback"> Looks good! </div>
                                                        </div>
                                                    </div>
                                                    <div className="col-lg-6">
                                                        <div className="form-group">
                                                            <label htmlFor="CVV" className="form-label">CVV</label>
                                                            <input type="text" className="form-control" id="CVV" placeholder="123" />
                                                            <div className="valid-feedback"> Looks good! </div>
                                                        </div>
                                                    </div>

                                                    <div className="col-12">
                                                        <div className="form-group">
                                                            <div className="form-check save_card">
                                                                <input className="form-check-input" type="checkbox" value="" id="flexCheckDefault" />
                                                                <label className="form-check-label" htmlFor="flexCheckDefault">
                                                                    Save card details
                                                                </label>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div className="col-12">
                                                        <button className="btn shared">
                                                            <span>Confirm  </span>
                                                        </button>
                                                        <p className="notes">Note that, your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our privacy policy.</p>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                        <div className="tab-pane fade" id="mobile" role="tabpanel" aria-labelledby="mobile-tab">
                                            <p className="text-white"> Mobile message</p>
                                        </div>
                                        <div className="tab-pane fade" id="NFT" role="tabpanel" aria-labelledby="NFT-tab">
                                            <p className="text-white">NFT</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="col-lg-5">
                                <div className="img">
                                    <img src={donaite} className="img-fluid" alt="donate" />
                                </div>
                            </div>
                        </div>
                    </ModalBody>
                </Form>
            </Modal>

            <Modal id="showModal" isOpen={amountPopup} toggle={showAmountPopup} modalClassName='donate_modal' centered size="m">
                <ModalHeader toggle={showAmountPopup}>
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <i className="fa fa-times" aria-hidden="true"></i>
                    </button>
                </ModalHeader>

                <Form
                    className="tablelist-form"
                    onSubmit={(e) => {
                        e.preventDefault();
                        validation.handleSubmit();
                        return false;
                    }}
                >
                    <ModalBody>
                        <div className="text ">
                            <div className="proForm">
                                <h4 className="title mb-3">{t("Invest in a Better Tomorrow: Donate to Our Cause.")}</h4>
                                <div className="form-group">
                                    <label htmlFor="card_number" className="form-label">{t("Please Enter amount")}</label>
                                    <Input
                                        name="amount"
                                        className="form-control"
                                        placeholder={t("amount")}
                                        type="text"
                                        onChange={validation.handleChange}
                                        onBlur={validation.handleBlur}
                                        value={validation.values.amount || ""}
                                        invalid={
                                            validation.touched.amount && validation.errors.amount ? true : false
                                        }
                                    />

                                    {validation.touched.amount && validation.errors.amount ? (
                                        <FormFeedback type="invalid">{validation.errors.amount}</FormFeedback>
                                    ) : null}
                                </div>
                                <div className="form-group">
                                    <button className='shared'>
                                        {project_loading ? <Spinner size="sm" className='me-2'> Loading... </Spinner> : null}
                                        <span>{t("Check out")}</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </ModalBody>
                </Form>
            </Modal>

            <Modal isOpen={smsPopup} toggle={showSmsPopup} modalClassName='donate_modal' centered size="lg">
                <ModalHeader toggle={showSmsPopup}>
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <i className="fa fa-times" aria-hidden="true"></i>
                    </button>
                </ModalHeader>
                <ModalBody>
                    <div className="text ">
                        <div className="proForm">
                            <h4 className="title mb-3">{t("Invest in a Better Tomorrow: Donate to Our Cause.")}</h4>

                            <div className="table-responsive table-card">
                                <table className="table align-middle table-nowrap">

                                    <tbody className="list form-check-all">

                                        {isSMSLoading ? <td colSpan="3">
                                            <IsLoading height={true} />
                                        </td>
                                            :
                                            (smsCodeList || []).map((item) => (
                                                <tr key={item.id}>
                                                    <td>{item.amount} AED	</td>
                                                    <td>Etisalat - Du	</td>
                                                    <td>
                                                        <a href={`sms:${item.recipient};?&body=${i18n.language === "ar" ? item?.msg_ar : item?.msg_en}`} className="btn primaryBtn shared">
                                                            <span>{t("Donate Now")}</span>
                                                        </a>
                                                    </td>
                                                </tr>
                                            ))}
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </ModalBody>
            </Modal>
        </>
    );
}

export default Home;