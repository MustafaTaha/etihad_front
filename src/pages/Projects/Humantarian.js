import React, { useCallback, useEffect, useState } from 'react';

import { Col, Input, Modal, ModalBody, ModalHeader, Form, Spinner, FormFeedback } from 'reactstrap';


import donaite from '../../styles/assets/images/donaite.png';
import search from '../../styles/assets/images/search_w.svg';


import Header from '../../Components/Common/Header';
import Footer from '../../Components/Common/Footer';

//redux
import { useSelector, useDispatch } from 'react-redux';


//import action
import {
    getSmscodes as onGetSmscodes,
    getHumanitarianProjects as onGetHumanitarianProjects,
    userPayment,
} from "../../slices/thunks";
import i18n from '../../i18n';
import { useTranslation } from 'react-i18next';
import { Link } from 'react-router-dom';
// Formik validation
import * as Yup from "yup";
import { useFormik } from "formik";

import IsLoading from "../../Components/Common/IsLoading";

const Humantarian = () => {
    const { t } = useTranslation();

    const dispatch = useDispatch();

    const { projectLists, user_info, project_loading, isLoading, smsCodeList, isSMSLoading } = useSelector((state) => ({

        projectLists: state.Projects.humanitarianProjectLists,
        smsCodeList: state.Projects.smsCodeList,
        user_info: state.Login.user.user,
        project_loading: state.Payment.loading,
        isLoading: state.Projects.isLoading,
        isSMSLoading: state.Projects.isSMSLoading,
    }));

    /*get single project id*/
    const [project_id, setProject_id] = useState(null);

    /*get single project*/
    const [project, setProject] = useState({});


    useEffect(() => {
        dispatch(onGetHumanitarianProjects());
    }, []);


    //search for project

    const [filteredProjects, setFilteredProjects] = useState(false);
    const [searchQuery, setSearchQuery] = useState(false);

    useEffect(() => {
        if (searchQuery) {
            let filtered = [];
            projectLists?.forEach((project) => {
                if (project?.title_en?.includes(searchQuery) || project?.title_ar?.includes(searchQuery)) {
                    filtered.push(project);
                }
            });
            setFilteredProjects(filtered);
        } else {
            setFilteredProjects(false);
        }
    }, [searchQuery]);




    /*popup for enter amount*/
    const [amountPopup, setAmountPopup] = useState(false);

    const showAmountPopup = useCallback((item) => {
        if (amountPopup) {
            setAmountPopup(false);
            // setLead(false);
            validation.resetForm();
        } else {
            setAmountPopup(true);
        }
    }, [amountPopup]);


    const validation = useFormik({
        // enableReinitialize : use this flag when initial values needs to be changed
        enableReinitialize: true,

        initialValues: {
            amount: '',
        },
        validationSchema: Yup.object({
            amount: Yup.number().typeError(t("Amount must be a number")).min(1, "can not be 0").required(t("Please Enter amount"))
        }),
        onSubmit: (values) => {

            let obj = {
                user_id: user_info ? user_info.id : 0,
                donation_id: project.id,
                donation_type: project.donation_type,
                amount: values.amount,
                device: "web",
                item_json: JSON.stringify(project),
                other_comments: ""
            }
            dispatch(userPayment(obj));
            if (!project_loading) {
                setTimeout(() => {
                    setAmountPopup(false);
                    validation.resetForm();
                }, 2000);
            }
        }
    });
    /*popup for SMS Codes*/
    const [smsPopup, setSmsPopup] = useState(false);

    const showSmsPopup = useCallback((id) => {
        if (smsPopup) {
            setSmsPopup(false);
        } else {
            dispatch(onGetSmscodes({ id: id, donation_type: "helping_case" }));
            setSmsPopup(true);
        }
    }, [smsPopup]);
    return (
        <>
            <Header />
            <main>
                <section className="bread_crumb">
                    <div className="container">
                        <h3 className="title">{t("Humantarian")}</h3>
                    </div>
                </section>

                <section className="our_projects p-t-b">
                    <div className="container">
                        <div className="row align-items-center">
                            <div className="col-lg-9">
                                <div className="sec_title" data-aos="fade-up">
                                    <h4 className="sub_title">{t("Our Projects")}</h4>
                                    <h3 className="title">{t("Together We Can Make a Difference: Our Vision for Change.")} </h3>
                                </div>

                            </div>
                            <div className="col-lg-3">
                                <div className="proForm">
                                    <div className="form-group">
                                        <Input type="text" className="form-control search" value={searchQuery ? searchQuery : ""} onChange={(e) => setSearchQuery(e.target.value)} placeholder={t("search")} />
                                        <img src={search} className="img-fluid icon" alt="search" />
                                    </div>
                                </ div>
                            </div>
                        </div>

                        <div className="row">
                            {isLoading ? <Col xl={12} >
                                <IsLoading height={true} />
                            </Col> :

                                (filteredProjects.length > 0 || projectLists.length > 0) ?
                                    (filteredProjects || projectLists || []).map((item) => (
                                        <Col xl={3} lg={4} md={6} key={item.id}>
                                            <div className="donaite-card" data-aos="fade-up">
                                                <a className="donaite-image">
                                                    <img src={item.thumbnail_url ? item.thumbnail_url : donaite} alt="donaite" className="img-fluid" />
                                                </a>
                                                <div className="text">
                                                    <div className="details">
                                                        <p className="num">{item.amount}</p>
                                                        <ul className="list-unstyled m_actions">
                                                            <li>
                                                                <button type='button' className="btn primaryBtn shared" onClick={() => { showAmountPopup(); setProject(item) }}>
                                                                    <span>{t("Donate Now")}</span>
                                                                </button>
                                                            </li>
                                                            <li>
                                                                <a className="btn blackBtn shared">
                                                                    <span>NFT </span>
                                                                </a>
                                                            </li>

                                                            <li>
                                                                <a className="btn secondaryBtn shared" onClick={() => { showSmsPopup(item.id); }} >
                                                                    <span>SMS </span>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                    <a className="name">{i18n.language === "ar" ? item?.title_ar : item?.title_en}</a>
                                                </div>
                                            </div>
                                        </Col>
                                    ))
                                    :
                                    <Col xl={12} >
                                        <p className='text-center text-white'>{t("there is no data to show")}</p>
                                    </Col>
                            }
                        </div>
                    </div>
                </section>

            </main>
            <Footer />

            <Modal id="showModal" isOpen={amountPopup} toggle={showAmountPopup} modalClassName='donate_modal' centered size="m">
                <ModalHeader toggle={showAmountPopup}>
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <i className="fa fa-times" aria-hidden="true"></i>
                    </button>
                </ModalHeader>

                <Form
                    className="tablelist-form"
                    onSubmit={(e) => {
                        e.preventDefault();
                        validation.handleSubmit();
                        return false;
                    }}
                >
                    <ModalBody>
                        <div className="text ">
                            <div className="proForm">
                                <h4 className="title mb-3">{t("Invest in a Better Tomorrow: Donate to Our Cause.")}</h4>
                                <div className="form-group">
                                    <label htmlFor="card_number" className="form-label">{t("Please Enter amount")}</label>
                                    <Input
                                        name="amount"
                                        className="form-control"
                                        placeholder={t("amount")}
                                        type="text"
                                        onChange={validation.handleChange}
                                        onBlur={validation.handleBlur}
                                        value={validation.values.amount || ""}
                                        invalid={
                                            validation.touched.amount && validation.errors.amount ? true : false
                                        }
                                    />

                                    {validation.touched.amount && validation.errors.amount ? (
                                        <FormFeedback type="invalid">{validation.errors.amount}</FormFeedback>
                                    ) : null}
                                </div>
                                <div className="form-group">
                                    <button className='shared'>
                                        {project_loading ? <Spinner size="sm" className='me-2'> Loading... </Spinner> : null}
                                        <span>{t("Check out")}</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </ModalBody>
                </Form>
            </Modal>
            <Modal isOpen={smsPopup} toggle={showSmsPopup} modalClassName='donate_modal' centered size="lg">
                <ModalHeader toggle={showSmsPopup}>
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <i className="fa fa-times" aria-hidden="true"></i>
                    </button>
                </ModalHeader>
                <ModalBody>
                    <div className="text ">
                        <div className="proForm">
                            <h4 className="title mb-3">{t("Invest in a Better Tomorrow: Donate to Our Cause.")}</h4>

                            <div className="table-responsive table-card">
                                <table className="table align-middle table-nowrap">

                                    <tbody className="list form-check-all">

                                        {isSMSLoading ? <td colSpan="3">
                                            <IsLoading height={true} />
                                        </td>
                                            :
                                            (smsCodeList || []).map((item) => (
                                                <tr key={item.id}>
                                                    <td>{item.amount} AED	</td>
                                                    <td>Etisalat - Du	</td>
                                                    <td>
                                                        <a href={`sms:${item.recipient};?&body=${i18n.language === "ar" ? item?.msg_ar : item?.msg_en}`} className="btn primaryBtn shared">
                                                            <span>{t("Donate Now")}</span>
                                                        </a>
                                                    </td>
                                                </tr>
                                            ))}
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                </ModalBody>
            </Modal>
        </>
    );
}

export default Humantarian;