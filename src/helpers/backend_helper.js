import { APIClient } from "./api_helper";

import * as url from "./url_helper";

const api = new APIClient();

// Gets the logged in user data from local session
export const getLoggedInUser = () => {
    const user = localStorage.getItem("user");
    if (user) return JSON.parse(user);
    return null;
};

// //is user is logged in
export const isUserAuthenticated = () => {
    return getLoggedInUser() !== null;
};

// Register Method
export const postFakeRegister = (data) => api.create(url.POST_FAKE_REGISTER, data);

// Login Method
export const postLogin = (data) => api.create(url.POST_LOGIN, data);

// register Method
export const postRegister = (data) => api.create(url.POST_REGISTER, data);

// checkEmail Method
export const checkEmail = (data) => api.create(url.CHECK_EMAIL, data);

// checkPhone Method
export const checkPhone = (data) => api.create(url.CHECK_PHONE, data);


// OTB send
export const OTB = (data) => api.create(url.OTB, data);

// postForgetPwd
export const postForgotpassword = (data) => api.create(url.POST_FORGOTPASSWORD, data);


// postChangePwd
export const postChangepassword = (data) => api.create(url.POST_CHANGEPASSWORD, data);

// postForgetPwd
export const postFakeForgetPwd = (data) => api.create(url.POST_FAKE_PASSWORD_FORGET, data);

// Edit profile
export const postJwtProfile = (data) => api.create(url.POST_EDIT_JWT_PROFILE, data);

export const postFakeProfile = (data) => api.update(url.POST_EDIT_PROFILE + "/" + data.idx, data);

// Register Method
export const postJwtRegister = (url, data) => {
    return api.create(url, data).catch((err) => {
        var message;
        if (err.response && err.response.status) {
            switch (err.response.status) {
                case 404:
                    message = "Sorry! the page you are looking for could not be found";
                    break;
                case 500:
                    message = "Sorry! something went wrong, please contact our support team";
                    break;
                case 401:
                    message = "Invalid credentials";
                    break;
                default:
                    message = err[1];
                    break;
            }
        }
        throw message;
    });
};

// Login Method
export const postJwtLogin = (data) => api.create(url.POST_FAKE_JWT_LOGIN, data);

// postForgetPwd
export const postJwtForgetPwd = (data) => api.create(url.POST_FAKE_JWT_PASSWORD_FORGET, data);

// postSocialLogin
export const postSocialLogin = (data) => api.create(url.SOCIAL_LOGIN, data);


//API Key
export const getAPIKey = () => api.get(url.GET_API_KEY);
// Home

// get getMain_slider List
export const getMain_sliderList = () => api.get(url.GET_MAIN_SLIDER_LIST);

// Project

// get Project list
export const getProjectList = (type) => api.get(url.GET_PROJECT_LIST + "?donation=" + type);
export const getDonationmaster = () => api.get(url.GET_DONTAIONMASTER);
export const getExternalProjects = () => api.get(url.GET_EXTERNALPROJECTS);
export const getOrphanList = (type) => api.get(url.GET_ORPHAN + "?gender=" + type);
export const getHumanitarianProjects = () => api.get(url.GET_HUMANITARIAN);





export const getSmscodes = (data) => api.create(url.GET_SMSCODE, data);



// postpayment
export const postPayment = (data) => api.create(url.POST_PAYMENT, data);


// News

// get News list
export const getNewsList = () => api.get(url.GET_NEWS_LIST);
// get News list
export const getNewsSingle = (data) => api.get(url.GET_NEWS_DETAILS + "/" + data);