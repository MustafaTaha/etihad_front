//REGISTER
export const POST_FAKE_REGISTER = "/auth/signup";

//LOGIN
export const POST_LOGIN = "/login";

//REGISTER
export const POST_REGISTER = "/registration";
export const CHECK_EMAIL = "/CheckIfEmailExistsWithAnotherUser";
export const CHECK_PHONE = "/CheckIfUserPhoneNumberExists";
export const OTB = "/sendsms";
export const POST_FORGOTPASSWORD = "/ForgotPassword";
export const POST_CHANGEPASSWORD = "/ChangePassword";


export const POST_FAKE_JWT_LOGIN = "/post-jwt-login";
export const POST_FAKE_PASSWORD_FORGET = "/auth/forgot-password";
export const POST_FAKE_JWT_PASSWORD_FORGET = "/jwt-forget-pwd";
export const SOCIAL_LOGIN = "/social-login";

//PROFILE
export const POST_EDIT_JWT_PROFILE = "/post-jwt-profile";
export const POST_EDIT_PROFILE = "/user";

//JOB APPLICATION
export const GET_API_KEY = "/api-key";

// Home

export const GET_MAIN_SLIDER_LIST = "/Campaign";

// Project list 
export const GET_PROJECT_LIST = "/DonationMaster";

export const GET_DONTAIONMASTER = "/DonationMaster";

export const GET_EXTERNALPROJECTS = "/ExternalProjects";

export const GET_ORPHAN = "/Orphan";
export const GET_HUMANITARIAN = "/HelpingCase";

export const GET_SMSCODE = "/smscode";
//user payment
export const POST_PAYMENT = "/networkpayment";

// News list  
export const GET_NEWS_LIST = "/news";
export const GET_NEWS_DETAILS = "/news"; 
