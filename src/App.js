import React, { useEffect, useLayoutEffect, useState } from "react";

import "./styles/assets/css/bootstrap.min.css";
import "./styles/assets/css/fontawesome5-all.css";
// Import Swiper styles
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
// import 'swiper/css/scrollbar';

import "./styles/custom.css";

//import Route
import Route from "./Routes";


import i18n from "./i18n";
import IsLoading from "./Components/Common/IsLoading";
import { useLocation } from "react-router-dom";
function App() {
    const [Loading, setLoading] = useState(true);

    // Extracts pathname property(key) from an object
    const { pathname } = useLocation();

    // Automatically scrolls to top whenever pathname changes
    useEffect(() => {
        window.scrollTo(0, 0);
    }, [pathname]);
    
    useLayoutEffect(() => {

        if (i18n.language === "en") import("./styles/assets/SCSS/en-us/layout.css");
        if (i18n.language === "ar") { import("./styles/assets/SCSS/ar-eg/layout-ar.css"); import("./styles/customAr.css") }
        if (i18n.language === "ar" || i18n.language == "en") import("./styles/custom.css");


        setTimeout(() => {
            setLoading(false);
        }, 1000);
    }, [i18n.language]);

    if (Loading)
        return (
            <IsLoading />
        );

    return (
        <Route />
    );
}

export default App;
