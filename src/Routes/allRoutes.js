import React from "react";
import { Navigate } from "react-router-dom";


 

//login
import Login from "../pages/Authentication/Login";
import PhoneLogin from "../pages/Authentication/PhoneLogin";
import ForgetPasswordPage from "../pages/Authentication/ForgetPassword";
import ChangePassword from "../pages/Authentication/ChangePassword";
import Logout from "../pages/Authentication/Logout";
import Register from "../pages/Authentication/Register";

import Home from "../pages/Home/Home";

//news
import News from "../pages/News/News";
import NewsSingle from "../pages/News/Single";

//organization
import Structure from "../pages/Organizational/Structure";
import Establishment from "../pages/Organizational/Establishment";
import Directors from "../pages/Organizational/Directors";
import Achievements from "../pages/Organizational/Achievements";
import Strategy from "../pages/Organizational/Strategy";
import Message from "../pages/Organizational/Message";
import Contact from "../pages/Organizational/Contact";

// Project
import CharityProjects from "../pages/Projects/CharityProjects"; 
import ExternalProjects from "../pages/Projects/ExternalProjects"; 
import SeasonalProjects from "../pages/Projects/SeasonalProjects"; 
import Humantarian from "../pages/Projects/Humantarian"; 
import OrphanMale from "../pages/Projects/OrphanMale"; 
import OrphanFemale from "../pages/Projects/OrphanFemale"; 


const authProtectedRoutes = [

    //  { path: "/apps-tasks-list-view", component: <TaskList /> },

    // this route should be at the end of all other routes
    // eslint-disable-next-line react/display-name
    { path: "/", exact: true, component: <Navigate to="/home" /> },
    { path: "*", component: <Navigate to="/home" /> },
];

const publicRoutes = [
    { path: "/", exact: true, component: <Home /> },
    // Authentication Page
    { path: "/logout", component: <Logout /> },
    { path: "/login", component: <Login /> },
    { path: "/phone-login", component: <PhoneLogin /> },
    { path: "/forgot-password", component: <ForgetPasswordPage /> },
    { path: "/change-password", component: <ChangePassword /> },
    { path: "/register", component: <Register /> },
    { path: "/home", component: <Home /> },

    { path: "/news", component: <News /> },
    { path: "/news/:id", component: <NewsSingle /> },

    { path: "/organization-structure", component: <Structure /> },
    { path: "/timeline", component: <Establishment /> },
    { path: "/directors", component: <Directors /> },
    { path: "/achievements", component: <Achievements /> },
    { path: "/organization-strategy", component: <Strategy /> },
    { path: "/word-of-board-firectors", component: <Message /> },
    { path: "/contact", component: <Contact /> },




   


    //Projects
    { path: "/internal-projects", component: <CharityProjects /> },
    { path: "/external-projects", component: <ExternalProjects /> },
    { path: "/seasonal-projects", component: <SeasonalProjects /> },
    { path: "/humantarian-projects", component: <Humantarian /> },
    { path: "/orphan-projects-male", component: <OrphanMale /> },
    { path: "/orphan-projects-female", component: <OrphanFemale /> },
    // { path: "/apps-projects-overview", component: <ProjectOverview /> },
    // { path: "/apps-projects-create", component: <CreateProject /> },
];

export { authProtectedRoutes, publicRoutes };
