import React, { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import Dropdown from 'react-bootstrap/Dropdown';
import { Link } from "react-router-dom";
import { Col, Row } from "reactstrap";
import footer_logo from '../../styles/assets/images/footer_logo.svg';
import arrow_right from '../../styles/assets/images/arrow-right.svg';
import i18n from "../../i18n";
const Header = ({ title, pageTitle }) => {
    const { t } = useTranslation();
    const changeLanguageAction = (lang) => {
        localStorage.setItem("I18N_LANGUAGE", lang);
        window.location.reload();
    }
    return (
        <footer>
            <div className="container">
                <div className="footer_up">
                    <div className="row">
                        <div className="col-lg-8">
                            <div className="text">
                                <div className="sec_title">
                                    <h3 className="title">{t("Help Us To Change Our Children’s Future")}</h3>
                                </div>
                                <p className="desc">
                                    {t("Your donation, no matter how small, can make a meaningful difference in the lives of those in need. Join us in creating positive change and donate today.")}

                                </p>
                                <a href="#projects" className="btn shared primaryBtn d-inline-flex"><span>{t("Donate Now")}</span></a>

                            </div>
                        </div>
                        <div className="col-lg-4">
                            <div className="map">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3604.2553020856303!2d55.4908423!3d25.396263400000002!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3e5f58759d35afd1%3A0x9b6b6b2d9dafb652!2z2YXYpNiz2LPYqSDYp9mE2KfYqtit2KfYryDYp9mE2K7Zitix2YrYqQ!5e0!3m2!1sen!2seg!4v1697274144579!5m2!1sen!2seg" width="600" height="250" style={{ border: "0" }} allowFullScreen={true} loading="lazy" referrerPolicy="no-referrer-when-downgrade"></iframe>
                                {/* <button className="btn shared primaryBtn"><span>{t("Call Now")}</span></button> */}
                                <a class="btn shared primaryBtn" href="tel:+971 56 664 4277"><span>{t("Call Now")}</span></a>

                            </div>
                        </div>

                    </div>
                </div>
                <div className="row">
                    <div className="col-lg-4   ">
                        <div className="footer_logo">
                            <a href="index.html">
                                <img src={footer_logo} className="img-fluid" alt="logo" />
                            </a>
                        </div>
                    </div>
                    <div className="col-lg-2 col-sm-6 ">
                        <div className="footer_links">
                            <h4>{t("WHAT WE DO")}</h4>

                            <ul className="list-unstyled">
                                <li> <Link className='drop-link' to={"/internal-projects"}> {t("Internal Projects")}</Link></li>
                                <li> <Link className='drop-link' to={"/external-projects"}> {t("External Projects")}</Link></li>
                                <li> <Link className='drop-link' to={"/seasonal-projects"}> {t("Seasonal Projects")}</Link></li>
                                <li> <Link className='drop-link' to={"/humantarian-projects"}>{t("Humantarian")}</Link> </li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-lg-2 col-sm-6">
                        <div className="footer_links">
                            <h4> {t("Media Center")}</h4>
                            <ul className="list-unstyled contact">
                                <li> <Link className='drop-link' to={"/news"}> {t("News")}</Link></li>
                                <li><Link className='drop-link' to={"/"}> {t("Images")}</Link></li>
                                <li><Link className='drop-link' to={"/"}> {t("Videos")}</Link></li>
                                <li><Link className='drop-link' to={"/"}> {t("Activities")} </Link></li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-lg-2 col-sm-6">
                        <div className="footer_links">
                            <h4>{t("WHO WE ARE")}</h4>
                            <ul className="list-unstyled contact">
                                <li><Link className='drop-link' to="/organization-structure" > {t("Organizational Structure")}</Link></li>
                                <li> <Link className='drop-link' to={"/directors"}> {t("Board of Directors")}</Link></li>
                                <li> <Link className='drop-link' to={"/achievements"}> {t("Our achievements")}</Link> </li>
                            </ul>
                        </div>
                    </div>
                    <div className="col-lg-2 col-sm-6">
                        <div className="footer_links">
                            <h4>{t("GET INVOLVED")}</h4>
                            <ul className="list-unstyled contact">
                                <li><Link to="/login"> {t("Login")} </Link></li>
                                {/* <li><Link to="/"> {t("Donate")} </Link>  </li> */}
                            </ul>
                        </div>
                    </div>
                </div>
                <div className="newsLetter row">
                    <div className="col-lg-4 col-11">
                        <form action="$" className="proForm">
                            <h4 className="title"> {t("Newsletter")} </h4>
                            <div className="form-group  ">
                                <input type="email" name="email" id="email" className="form-control" placeholder={t("email address")} />
                                <img src={arrow_right} className="img-fluid icon" alt="arrow" />
                            </div>
                            <p className="desc">
                                {t("By subscribing to our news letter, you agree to our terms & conditions.")}
                            </p>
                            <button className="btn shared primaryBtn" type="button"><span>{t("Submit")}</span></button>
                        </form>
                    </div>
                    <div className="col-lg-8">
                        <div className="privacy">
                            <ul className="list-unstyled privacy_ul">
                                <li> <a href="#">{t("Privacy Policy")} </a> </li>
                                <li> <a href="#">{t("Contact US")}</a> </li>
                                {i18n.language === "en" && <li> <a onClick={() => changeLanguageAction("ar")} style={{ fontFamily: 'system-ui' }} >عربى</a> </li>}
                                {i18n.language === "ar" && <li> <a onClick={() => changeLanguageAction("en")}>English</a> </li>}

                            </ul>
                            <div className="social">
                                <ul className="list-unstyled">
                                    <li>
                                        <a target="_blank" href="https://www.youtube.com/channel/UCpXCNb-HmlKfNHfSiE4CIeA"><i className="fab fa-youtube"></i></a>
                                    </li>
                                    <li>
                                        <a target="_blank" href="https://www.snapchat.com/add/miecajman"><i className="fab fa-snapchat-square"></i></a>
                                    </li>
                                    <li>
                                        <a target="_blank" href="https://wa.me/971566644277"><i className="fab fa-whatsapp"></i></a>
                                    </li>
                                    <li>
                                        <a target="_blank" href="https://twitter.com/etihadcharity/status/1388782686353887233"><i className="fab fa-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a target="_blank" href="https://www.instagram.com/etihadcharity/"><i className="fab fa-instagram"></i></a>
                                    </li>
                                    <li>
                                        <a target="_blank" href="https://www.facebook.com/etihadcharity/"><i className="fab fa-facebook"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="toTop">
                            <div className="back-to-top-btn" onClick={() => window.scrollTo(0, 0)}><i className="fas fa-arrow-up"></i></div>
                        </div>
                    </div>
                </div>



            </div>
        </footer>
    );
};

export default Header;
