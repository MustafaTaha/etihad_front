import React, { useCallback, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import Dropdown from 'react-bootstrap/Dropdown';
import { Link, Navigate } from "react-router-dom";
import { Col, Input, Modal, ModalBody, ModalHeader, Form, Spinner, FormFeedback } from 'reactstrap';
import logo from '../../styles/assets/images/Logo.svg';
import { toast } from "react-toastify";
import { logoutUser } from "../../slices/thunks";
import i18n from "../../i18n";

// Formik validation
import * as Yup from "yup";
import { useFormik } from "formik";
//redux
import { useSelector, useDispatch } from 'react-redux';


//import action
import { userPayment } from "../../slices/thunks";
import { getDonationmaster } from "../../slices/thunks";

const Header = ({ title, pageTitle }) => {
    const { t } = useTranslation();
    const changeLanguageAction = (lang) => {
        localStorage.setItem("I18N_LANGUAGE", lang);
        window.location.reload();
    }
    const [scroll, setScroll] = useState(false);
    const [menu, setMenu] = useState(false);
    useEffect(() => {
        window.addEventListener("scroll", () => {
            setScroll(window.scrollY > 70);
        });
    }, []);
    const handleLogout = () => {
        sessionStorage.removeItem("authUser");
        localStorage.removeItem("authUser");
        // useEffect(() => {
        //     dispatch(logoutUser());
        // }, [dispatch]);
        toast.success(t("successfully logout"))
    }

    /*get single project*/
    const [project, setProject] = useState({});

    const dispatch = useDispatch();
    const handleQuickDonation = (e) => {
         e.preventDefault();
        dispatch(getDonationmaster());
        setProject(donationmaster?.internal_project);
        showAmountPopup();
    }
    const { donationmaster, user_info, project_loading, isLoading } = useSelector((state) => ({
        donationmaster: state.Projects.donationmaster,
        user_info: state.Login.user.user,
        project_loading: state.Payment.loading,
        isLoading: state.Projects.isLoading,
    }));
    /*popup for enter amount*/
    const [amountPopup, setAmountPopup] = useState(false);

    const showAmountPopup = useCallback((item) => {
        if (amountPopup) {
            setAmountPopup(false);
            // setLead(false);
            validation.resetForm();
        } else {
            setAmountPopup(true);
        }
    }, [amountPopup]);


    const validation = useFormik({
        // enableReinitialize : use this flag when initial values needs to be changed
        enableReinitialize: true,

        initialValues: {
            amount: '',
        },
        validationSchema: Yup.object({
            amount: Yup.number().typeError(t("Amount must be a number")).min(1, "can not be 0").required(t("Please Enter amount"))
        }),
        onSubmit: (values) => {
            let obj = {
                user_id: user_info ? user_info.id : 0,
                donation_type: donationmaster?.campaign?.donation_type || donationmaster?.humanitarian?.donation_type || donationmaster?.internal_project?.donation_type || donationmaster?.orphan?.donation_type || donationmaster?.seasonal_project?.donation_type,
                donation_id: donationmaster?.campaign?.id || donationmaster?.humanitarian?.id || donationmaster?.internal_project?.id || donationmaster?.orphan?.id || donationmaster?.seasonal_project?.id,
                amount: values.amount,
                device: "web",
                item_json: JSON.stringify(donationmaster?.campaign || donationmaster?.humanitarian || donationmaster?.internal_project || donationmaster?.orphan || donationmaster?.seasonal_project),
                other_comments: ""
            }
            dispatch(userPayment(obj));
            if (!project_loading) {
                setTimeout(() => {
                    setAmountPopup(false);
                    validation.resetForm();
                }, 2000);
            }
        }
    });


    return (
        <>
            <a href="#" className="quick_donate_link" onClick={(e) => { handleQuickDonation(e) }}><span>{t("Quick Donate")}</span></a>
            <header className={" main_header " + (scroll ? " header-fixed" : "")}>
                <div className="header_navbar">
                    <div className="container">
                        <div className="row">
                            <div className="col-6 col-md-3  order-lg-0 order-1">
                                <Link to="/" className="logo">
                                    <img src={logo} className="img-fluid" alt="logo" />
                                </Link>
                            </div>
                            <div className="col-6 col-md-9 order-lg-1 order-0">
                                <nav className="navbar navbar-expand-lg navbar-light">
                                    <div className={"hamburger" + (menu ? " is-active" : "")} onClick={() => setMenu(!menu)} data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo03"
                                        aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
                                        <span className="line"></span>
                                        <span className="line"></span>
                                        <span className="line"></span>
                                    </div>

                                    <div className={"collapse navbar-collapse " + (menu ? " show" : "")} id="navbarTogglerDemo03">

                                        <ul className="navbar-nav ">
                                            <li className="nav-item">
                                                <Link to={"/"} className="nav-link">{t("Home")}</Link>
                                            </li>
                                            <Dropdown className="nav-item">
                                                <Dropdown.Toggle className='nav-link' >
                                                    {t("Our organization")}
                                                    <i className="fa fa-chevron-down" aria-hidden="true"></i>
                                                </Dropdown.Toggle>

                                                <Dropdown.Menu>
                                                    <li> <Link className='drop-link' to="/timeline" > {t("Establishment")}</Link> </li>
                                                    <li> <Link className='drop-link' to="/organization-structure" > {t("Organizational Structure")}</Link> </li>
                                                    <li><Link className='drop-link' to={"/directors"}> {t("Board of Directors")}</Link></li>
                                                    <li> <Link className='drop-link' to={"/achievements"}> {t("Our achievements")}</Link></li>
                                                    <li> <Link className='drop-link' to={"/organization-strategy"}> {t("Vision and mission")}</Link></li>
                                                    <li><Link className='drop-link' to={"/contact"}> {t("Contact us")}</Link></li>
                                                </Dropdown.Menu>
                                            </Dropdown>

                                            <Dropdown className="nav-item">
                                                <Dropdown.Toggle className='nav-link' >
                                                    {t("charity projects")}
                                                    <i className="fa fa-chevron-down" aria-hidden="true"></i>
                                                </Dropdown.Toggle>

                                                <Dropdown.Menu>
                                                    <li><Link className='drop-link' to={"/internal-projects"}> {t("Internal Projects")}</Link></li>
                                                    <li> <Link className='drop-link' to={"/external-projects"}> {t("External Projects")}</Link></li>
                                                    <li> <Link className='drop-link' to={"/seasonal-projects"}> {t("Seasonal Projects")}</Link></li>
                                                </Dropdown.Menu>
                                            </Dropdown>

                                            <li className="nav-item">
                                                <Link className='nav-link' to={"/humantarian-projects"}>{t("Humantarian")}</Link>
                                            </li>

                                            <Dropdown className="nav-item">
                                                <Dropdown.Toggle className='nav-link' >
                                                    {t("Orphan")}
                                                    <i className="fa fa-chevron-down" aria-hidden="true"></i>
                                                </Dropdown.Toggle>

                                                <Dropdown.Menu>
                                                    <li><Link className='drop-link' to={"/orphan-projects-male"}> {t("male orphans")}</Link></li>
                                                    <li> <Link className='drop-link' to={"/orphan-projects-female"}> {t("female orphans")}</Link></li>
                                                </Dropdown.Menu>
                                            </Dropdown>


                                            <Dropdown className="nav-item">
                                                <Dropdown.Toggle className='nav-link' >
                                                    {t("Media center")}
                                                    <i className="fa fa-chevron-down" aria-hidden="true"></i>
                                                </Dropdown.Toggle>

                                                <Dropdown.Menu>
                                                    <li><Link className='drop-link' to={"/news"}> {t("News")}</Link></li>
                                                    <li><Link className='drop-link' to={"/"}> {t("Images")}</Link></li>
                                                    <li><Link className='drop-link' to={"/"}> {t("Videos")}</Link></li>
                                                    <li><Link className='drop-link' to={"/"}> {t("Activities")} </Link></li>
                                                </Dropdown.Menu>
                                            </Dropdown>
                                            {i18n.language === "en" && <li className="nav-item"> <a className="nav-link" onClick={() => changeLanguageAction("ar")} style={{ fontFamily: 'system-ui', cursor: "pointer" }} >عربى</a> </li>}
                                            {i18n.language === "ar" && <li className="nav-item"> <a className="nav-link" onClick={() => changeLanguageAction("en")} style={{ cursor: "pointer" }}>English</a> </li>}


                                            <li className="nav-item">
                                                {sessionStorage.getItem("authUser") ? <Link to="/" onClick={handleLogout} className="shared"><span>{t("Logout")}</span></Link> : <Link to="/login" className="shared"><span>{t("Login")}</span></Link>}

                                            </li>
                                        </ul>
                                    </div>
                                </nav>

                            </div>
                        </div>
                    </div>
                </div>

            </header>

            <div className={"over-nav " + (menu ? " is-active" : "")} onClick={() => setMenu(!menu)} />

            <Modal id="showModal" isOpen={amountPopup} toggle={showAmountPopup} modalClassName='donate_modal' centered size="m">
                <ModalHeader toggle={showAmountPopup}>
                    <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close">
                        <i className="fa fa-times" aria-hidden="true"></i>
                    </button>
                </ModalHeader>

                <Form
                    className="tablelist-form"
                    onSubmit={(e) => {
                        e.preventDefault();
                        validation.handleSubmit();
                        return false;
                    }}
                >
                    <ModalBody>
                        <div className="text ">
                            <div className="proForm">
                                <h4 className="title mb-3">{t("Invest in a Better Tomorrow: Donate to Our Cause.")}</h4>
                                <div className="form-group">
                                    <label htmlFor="card_number" className="form-label">{t("Please Enter amount")}</label>
                                    <Input
                                        name="amount"
                                        className="form-control"
                                        placeholder={t("amount")}
                                        type="text"
                                        onChange={validation.handleChange}
                                        onBlur={validation.handleBlur}
                                        value={validation.values.amount || ""}
                                        invalid={
                                            validation.touched.amount && validation.errors.amount ? true : false
                                        }
                                    />

                                    {validation.touched.amount && validation.errors.amount ? (
                                        <FormFeedback type="invalid">{validation.errors.amount}</FormFeedback>
                                    ) : null}
                                </div>
                                <div className="form-group">
                                    <button className='shared' type="submit">
                                        {project_loading ? <Spinner size="sm" className='me-2'> Loading... </Spinner> : null}
                                        <span>{t("Check out")}</span>
                                    </button>
                                </div>
                            </div>
                        </div>
                    </ModalBody>
                </Form>
            </Modal>
        </>
    );
};

export default Header;
