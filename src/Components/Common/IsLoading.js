import React from "react";

const isLoading = ({ height }) => {
    return (
        <div id="preloader" className="spinner_center" style={{ height: height ? "auto" : null }}>
            <div id="status">
                <div className="spinner-border text-warning avatar-sm" role="status">
                    <span className="visually-hidden">Loading...</span>
                </div>
            </div>
        </div>
    );
};

export default isLoading;
