$(document).ready(function () {
    AOS.init({
        duration: 1600,
    })

    $('.js-example-basic-single').select2();

    $('.dropdown').on('show.bs.dropdown', function () {
        $(this).find('.dropdown-menu').slideDown();
    });

    $('.dropdown').on('hide.bs.dropdown', function () {
        $(this).find('.dropdown-menu').slideUp();
    });

    // navbar fixes top after scroll
    // var prevScrollpos = window.pageYOffset;
    // $(window).bind('scroll', function () {
    //     var navHeight = $('.main_header').height();
    //     if ($(window).scrollTop() >= navHeight) {
    //         $('.main_header').addClass('header-fixed');
    //         $('.main_header .top_header').slideUp()
    //     } else {
    //         $('.main_header').removeClass('header-fixed');
    //         $('.main_header .top_header').slideDown()

    //     }

    //     var currentScrollPos = window.pageYOffset;
    //     if (prevScrollpos > currentScrollPos) {
    //         $(".main_header").css('top', '0');
    //     } else {
    //         $(".main_header").css('top', '-90px');
    //     }
    //     prevScrollpos = currentScrollPos;
    // });
    $(window).bind('scroll', function () {
        var navHeight = $('.main_header').height();
        if ($(window).scrollTop() >= navHeight) {
            $('.main_header').addClass('header-fixed');
            $('.main_header .top_header').slideUp()
        } else {
            $('.main_header').removeClass('header-fixed');
            $('.main_header .top_header').slideDown()

        }
    });

    if ($('.count-number')[0]) {

        function detect_visibility() {
    
            var top_of_element = $(".count-number").offset().top;
            var bottom_of_element = $(".count-number").offset().top + $(".count-number").outerHeight();
            var bottom_of_screen = $(window).scrollTop() + $(window).innerHeight();
            var top_of_screen = $(window).scrollTop();
    
            if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)) {
                // Element is visible write your codes here
                // You can add animation or other codes here
                $('.count-number').each(function () {
                    var $this = $(this),
                        countTo = $this.attr('data-count');
    
                    $({ countNum: $this.text() }).animate({
                        countNum: countTo
                    },
                        {
    
                            duration: 5000,
                            easing: 'linear',
                            step: function () {
                                $this.text(Math.floor(this.countNum));
                            },
                            complete: function () {
                                $this.text(this.countNum);
                            }
    
                        });
    
    
                });
            } else {
                // the element is not visible, do something else
            }
    
        }
    
        // detect when element gets visible on scroll
        $(window).scroll(function () {
            detect_visibility();
        });
    
        // detect when screen opens for first time
        detect_visibility();
    }

    /* close cpllaps navbar*/
    $('.close-nav').click(function (e) {
        e.preventDefault();
        $('.navbar-collapse').removeClass('collapse show');
        $('.over-nav').css('display', 'none');
    });
    $('.searchIcon').click(function () {
        $('.search_box').slideToggle();
        $('.over-search').toggleClass('is-active');
    });
    $('.over-search').click(function () {
        $('.search_box').slideToggle();
        $(this).toggleClass('is-active');
    });
    $('.hamburger').click(function () {
        $(this).toggleClass("is-active");
        $('.over-nav').toggleClass('is-active');
    });
    $('.over-nav').click(function () {
        $('.navbar-collapse').removeClass('collapse show');
        $('.hamburger').toggleClass("is-active");
        $(this).toggleClass('is-active');
    });





    /* to top*/
    // $("body").append('<div class="back-to-top-btn"><i class="fas fa-arrow-up"></i></div>');
    // $(window).scroll(function () {
    //     if ($(this).scrollTop() != 0) {
    //         $(".back-to-top-btn").fadeIn();
    //     } else {
    //         $(".back-to-top-btn").fadeOut();
    //     }
    // });
    $(".back-to-top-btn").on("click", function () {
        $("html, body").animate({ scrollTop: 0 });
        return false;
    });





 

    /**
   *   slider
   */
    var mainSlider = new Swiper(".main-slider .swiper", {
        slidesPerView: 1,
        speed: 2000,
        autoplay:
        {
            delay: 5000,
        },
        loop: true,
        spaceBetween: 5,
        pagination: {
            el: ".swiper-pagination",
            clickable: true,

        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        },
        breakpoints: {
            640: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            768: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            1024: {
                slidesPerView: 1,
                spaceBetween: 20,
            },
            1200: {
                slidesPerView: 1,
                spaceBetween: 20,
            }
        },
    });
    var clinet = new Swiper('.clinets .first', {
        slidesPerView: 1,
        speed: 1000,
        autoplay:
        {
            delay: 1000,
        },
        loop: true,
        spaceBetween: 5,
        breakpoints: {
            425: {
                slidesPerView: 2
            },
            576:{
                slidesPerView: 3

            },
            740: {
                slidesPerView: 3
            },
            960: {
                slidesPerView: 5
            },
            1280: {
                slidesPerView: 8
            }
        }
    })

    var clinet2 = new Swiper('.clinets .second', {
        slidesPerView: 1,
        speed: 1000,
        autoplay:
        {
            delay: 1000,
        },
        loop: true,
        spaceBetween: 5,
        breakpoints: {
            425: {
                slidesPerView: 2
            },
            576:{
                slidesPerView: 3

            },
            740: {
                slidesPerView: 3
            },
            960: {
                slidesPerView: 5
            },
            1280: {
                slidesPerView: 8
            }
        }
    })
    

    $(document).on('change', '.file-upload', function () {
        // console.log(this.files[0]);
        let videoSrc = $(this).parent().parent().find('.profile-pic');
        if (this.files && this.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                videoSrc.attr('src', e.target.result);
            }
            reader.readAsDataURL(this.files[0]);
        }
    });

    $(".upload-button").on('click', function () {
        $(this).parent().find('.file-upload').click();
    });

    //input type file for upload file name
    $('.file-upload').change(function () {
        var i = $(this).prev('label').clone();
        var file = $(this)[0].files[0].name;
        $(this).prev('label').text(file);
    });


    $(".showPassword").click(function () {
        // $(this).toggleClass("fa-eye fa-eye-slash");
        input = $(this).parent().find("input");
        if (input.attr("type") == "password") {
            input.attr("type", "text");
        } else {
            input.attr("type", "password");
        }
    });
    $(document).on('click', '.page-link-scroll-to-top', function (e) {
        $("html, body").animate({ scrollTop: 0 }, "slow");
        return false;
    });
});






$(document).on('click', '.remove', function (e) {
    e.preventDefault();
    $(this).parent().remove();
});





jQuery(document).ready(function () {
    ImgUpload();
});
var loading = $(".loader-container");
loading.delay(loading.attr("delay-hide")).fadeOut(2000);
function ImgUpload() {
    var imgWrap = "";
    var imgArray = [];

    $('.upload__inputfile').each(function () {
        $(this).on('change', function (e) {
            imgWrap = $(this).closest('.upload__box').find('.upload__img-wrap');
            var maxLength = $(this).attr('data-max_length');

            var files = e.target.files;
            var filesArr = Array.prototype.slice.call(files);
            var iterator = 0;
            filesArr.forEach(function (f, index) {

                if (!f.type.match('image.*')) {
                    return;
                }

                if (imgArray.length > maxLength) {
                    return false
                } else {
                    var len = 0;
                    for (var i = 0; i < imgArray.length; i++) {
                        if (imgArray[i] !== undefined) {
                            len++;
                        }
                    }
                    if (len > maxLength) {
                        return false;
                    } else {
                        imgArray.push(f);

                        var reader = new FileReader();
                        reader.onload = function (e) {
                            var html = "<div class='upload__img-box'><div style='background-image: url(" + e.target.result + ")' data-number='" + $(".upload__img-close").length + "' data-file='" + f.name + "' class='img-bg'><div class='upload__img-close'></div></div></div>";
                            imgWrap.append(html);
                            iterator++;
                        }
                        reader.readAsDataURL(f);
                    }
                }
            });
        });
    });

    $('body').on('click', ".upload__img-close", function (e) {
        var file = $(this).parent().data("file");
        for (var i = 0; i < imgArray.length; i++) {
            if (imgArray[i].name === file) {
                imgArray.splice(i, 1);
                break;
            }
        }
        $(this).parent().parent().remove();
    });

} const images = document.querySelectorAll("[data-src]");
const imgOptions = {
    threshold: 0,
    rootMargin: "0px 0px 300px 0px"
}
function preloadImage(img) {
    const src = img.getAttribute('data-src');
    if (!src) {
        return;
    }
    img.src = src
}
const imgObserver = new IntersectionObserver((entries, imgObserver) => {
    entries.forEach(entry => {
        if (!entry.isIntersecting) {
            return;
        } else {
            preloadImage(entry.target)
            imgObserver.unobserve(entry.target)
        }
    })
}, imgOptions)
images.forEach(image => {
    imgObserver.observe(image);
})
