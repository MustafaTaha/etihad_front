import { createSlice } from "@reduxjs/toolkit";
import { getMain_sliderList as getMain_sliderList } from './thunk';
export const initialState = {
    main_sliderList: [],
    error: {},
};


const ProjectsSlice = createSlice({
    name: 'Main_sliderSlice',
    initialState,
    reducer: {},
    extraReducers: (builder) => {
        builder.addCase(getMain_sliderList.fulfilled, (state, action) => {
            state.main_sliderList = action.payload;
        });
        builder.addCase(getMain_sliderList.rejected, (state, action) => {
            state.error = action.payload?.error || null;
        }); 
    }
});

export default ProjectsSlice.reducer;