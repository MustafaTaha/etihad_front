import { createAsyncThunk } from "@reduxjs/toolkit";  
 
import {
    getMain_sliderList as getMain_sliderListApi, 
} from "../../../helpers/backend_helper";

export const getMain_sliderList = createAsyncThunk("main_slider/getMain_sliderList", async () => {
    try {
        const response = getMain_sliderListApi();
        return response;
    } catch (error) { 
        return error;
    }
});
 