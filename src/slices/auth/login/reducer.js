import { createSlice } from "@reduxjs/toolkit";

export const initialState = {
    user: {},
    error: "", // for error message
    loading: false,
    isUserLogout: false,
    errorMsg: false, // for error
};

const loginSlice = createSlice({
    name: "login",
    initialState,
    reducers: {
        apiError(state, action) {
            state.error = "Wrong username or password.";
            // state.error = action.payload.login_type == "email" ? "Wrong username or password." : "Wrong phone number or password";
            state.loading = true;
            state.isUserLogout = false;
            state.errorMsg = true;

        },
        loginSuccess(state, action) {
            state.user = action.payload;
            state.loading = false;
            state.errorMsg = false;
        },
        logoutUserSuccess(state, action) {
            state.isUserLogout = true;
            state.user = {};
        },
        reset_login_flag(state) {
            state.error = null;
            state.loading = false;
            state.errorMsg = false;
        },
        progressLoading(state) {
            state.loading = true;
        },
    },
});

export const { apiError, loginSuccess, logoutUserSuccess, reset_login_flag, progressLoading } = loginSlice.actions;

export default loginSlice.reducer;
