//Include Both Helper File with needed methods
import { getFirebaseBackend } from "../../../helpers/firebase_helper";
import { postLogin, postJwtLogin } from "../../../helpers/backend_helper";

import { loginSuccess, logoutUserSuccess, apiError, reset_login_flag, progressLoading } from "./reducer";
import { setAuthorization } from "../../../helpers/api_helper";


export const loginUser = (user, history) => async (dispatch) => {
    try {
        dispatch(progressLoading());
        let response;
        if (user.login_type == "email") {
            response = postLogin({
                login_type: user.login_type,
                email_id: user.email_id,
                password: user.password,
                social_media_name : "own"
            });
        } else if (user.login_type == "phone_number") {
            response = postLogin({
                country_code: user.country_code,
                phone_number: user.phone_number,
                login_type: user.login_type,
                password: user.password,
                social_media_name : "own"
            });
        }

        var data = await response;

        if (data) {
            if (data.status === "success") {
                sessionStorage.setItem("authUser", JSON.stringify(data));
                localStorage.setItem("authUser", JSON.stringify(data.user));
                // setAuthorization(data?.data?.token);
                setAuthorization(true);

            }
            if (process.env.REACT_APP_DEFAULTAUTH === "fake") {
                var finallogin = JSON.stringify(data);
                finallogin = JSON.parse(finallogin);
                // data = finallogin.data;
                if (finallogin.status === "success") {
                    dispatch(loginSuccess(finallogin));
                    history("/home");
                } else {
                    dispatch(apiError(finallogin, user?.login_type));
                }
            } else {
                dispatch(loginSuccess(data));
                history("/home");
            }
        }
    } catch (error) {
        dispatch(apiError(error, user?.login_type));
    }
};

export const logoutUser = () => async (dispatch) => {
    try {
        sessionStorage.removeItem("authUser");
        let fireBaseBackend = getFirebaseBackend();
        if (process.env.REACT_APP_DEFAULTAUTH === "firebase") {
            const response = fireBaseBackend.logout;
            dispatch(logoutUserSuccess(response));
        } else {
            dispatch(logoutUserSuccess(true));
        }
    } catch (error) {
        dispatch(apiError(error));
    }
};

export const socialLogin = (type, history) => async (dispatch) => {
    try {
        let response;

        if (process.env.REACT_APP_DEFAULTAUTH === "firebase") {
            const fireBaseBackend = getFirebaseBackend();
            response = fireBaseBackend.socialLoginUser(type);
        }
        //  else {
        //   response = postSocialLogin(data);
        // }

        const socialdata = await response;
        if (socialdata) {
            sessionStorage.setItem("authUser", JSON.stringify(response));
            dispatch(loginSuccess(response));
            history("/home");
        }
    } catch (error) {
        dispatch(apiError(error));
    }
};

export const resetLoginFlag = () => async (dispatch) => {
    try {
        const response = dispatch(reset_login_flag());
        return response;
    } catch (error) {
        dispatch(apiError(error));
    }
};
