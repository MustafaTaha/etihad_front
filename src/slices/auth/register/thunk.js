//Include Both Helper File with needed methods
import { postRegister, checkEmail, OTB, checkPhone } from "../../../helpers/backend_helper";

// action
import { registerUserSuccessful, registerUserFailed, resetRegisterFlagChange, apiErrorChange, progressLoading, otbSuccessful, emailExist, emailNotExist, phoneExist, phoneNotExist } from "./reducer";
 

// Is user register successfull then direct plot user in redux.
export const registerUser = (user) => async (dispatch) => {
  try {
    dispatch(progressLoading());
    let response;
    response = postRegister(user);

    var data = await response; 
    if (data) {
      dispatch(registerUserSuccessful(data));
    } else {
      dispatch(registerUserFailed(data.message));
    }
    return response
  } catch (error) {
    dispatch(registerUserFailed(error));
  }
};

// if email exist successfull then direct 
export const checkEmailExist = (email) => async (dispatch) => {
  try {
    // dispatch(progressLoading());
    let response;
    response = checkEmail({ email_id: email });

    var data = await response; 
    if (data === true) {
      dispatch(emailExist(true))
    } else {
      dispatch(emailNotExist(false))
    }

  } catch (error) {
    dispatch(registerUserFailed(error));
  }
};

// if phone exist successfull then direct 
export const checkPhoneExist = (phone_number, country_code) => async (dispatch) => {
  try {
    // dispatch(progressLoading());
    let response;
    response = checkPhone({ phone_number, country_code });

    var data = await response; 
    if (data === true) {
      dispatch(phoneExist(true))
    } else {
      dispatch(phoneNotExist(false))
    } 


  } catch (error) {
    dispatch(registerUserFailed(error));
  }
};

// if checkOTB successfull  
export const checkOTB = (obj) => async (dispatch) => {
  try {
    // dispatch(progressLoading());
    let response;
    response = OTB(obj);
    var data = await response;
    dispatch(otbSuccessful(data))
  } catch (error) {
    dispatch(registerUserFailed(error));
  }
};

export const resetRegisterFlag = () => {
  try {
    const response = resetRegisterFlagChange();
    return response;
  } catch (error) {
    return error;
  }
};

export const apiError = () => {
  try {
    const response = apiErrorChange();
    return response;
  } catch (error) {
    return error;
  }
};