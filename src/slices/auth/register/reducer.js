import { createSlice } from "@reduxjs/toolkit";
import { registerUser } from "./thunk";

export const initialState = {
  registrationError: null,
  message: null,
  loading: false,
  user: null,
  success: false,
  error: false,
  otb: ''
};

const registerSlice = createSlice({
  name: "register",
  initialState,
  reducers: {
    registerUserSuccessful(state, action) {
      state.user = action.payload;
      state.loading = false;
      state.success = true;
      state.registrationError = null;
    },
    registerUserFailed(state, action) {
      state.user = null;
      state.loading = false;
      state.registrationError = action?.payload;
      state.error = true;
    },
    resetRegisterFlagChange(state) {
      state.success = false;
      state.error = false;
    },
    apiErrorChange(state, action) {
      state.error = action.payload;
      state.loading = false;
      state.isUserLogout = false;
    },
    progressLoading(state) {
      state.loading = true;
    },
    otbSuccessful(state, action) {
      state.otb = action.payload;
    },
    emailExist(state, action) {
      state.emailExist = action.payload;
    },
    emailNotExist(state, action) {
      state.emailExist = action.payload;
    },
    phoneExist(state, action) {
      state.phoneExist = action.payload;
    },
    phoneNotExist(state, action) {
      state.phoneExist = action.payload;
    },

  }, 
});

export const {
  registerUserSuccessful,
  registerUserFailed,
  resetRegisterFlagChange,
  apiErrorChange,
  progressLoading,
  otbSuccessful,
  emailExist,
  emailNotExist,
  phoneExist,
  phoneNotExist
} = registerSlice.actions;

export default registerSlice.reducer;