import { postChangepassword } from "../../../helpers/backend_helper";

import { userChangePasswordSuccess, userChangePasswordError, progressLoading } from "./reducer"


export const userChangePassword = (obj, history) => async (dispatch) => {
    try {
        dispatch(progressLoading());
        let response;
        response = postChangepassword(obj);

        var data = await response;
        if (data) {
            dispatch(userChangePasswordSuccess(data))
        } else {
            dispatch(userChangePasswordError(data));
        }
        return response
    } catch (forgetError) {
        dispatch(userChangePasswordError(forgetError))
    }
}
 
 