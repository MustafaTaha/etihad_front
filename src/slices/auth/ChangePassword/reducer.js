import { createSlice } from "@reduxjs/toolkit";

export const initialState = {
  forgetSuccessMsg: null,
  forgetError: null,
  loading: false,
  user: null,
  success: false,
  error: false,
  otb: ''
};

const changePasswordSlice = createSlice({
  name: "changePassword",
  initialState,
  reducers: {
    userChangePasswordSuccess(state, action) {
      // state.forgetSuccessMsg = action.payload;
      state.user = action.payload;
      state.loading = false;
      state.success = true;
      // state.registrationError = null;
    },
    userChangePasswordError(state, action) {
      // state.forgetError = action.payload
      state.user = null;
      state.loading = false;
      // state.registrationError = action?.payload;
      state.error = true;
    },
    progressLoading(state) {
      state.loading = true;
    },
    setIsSuccess_CP(state, action) {
      state.success = action.payload;
    },
  },
});

export const {
  userChangePasswordSuccess,
  userChangePasswordError,
  progressLoading,
  setIsSuccess_CP

} = changePasswordSlice.actions

export default changePasswordSlice.reducer;
