import { postForgotpassword, OTB, checkPhone } from "../../../helpers/backend_helper";

import { userForgetPasswordSuccess, userForgetPasswordError, phoneExist_FP, phoneNotExist_FP, otbSuccessful, progressLoading } from "./reducer"


export const userForgetPassword = (user, history) => async (dispatch) => {
    try {
        dispatch(progressLoading());
        let response;
        response = postForgotpassword(user);

        var data = await response;
        if (data) {
            dispatch(userForgetPasswordSuccess(data)) 
        } else {
            dispatch(userForgetPasswordError(data));
        }
        return response
    } catch (forgetError) {
        dispatch(userForgetPasswordError(forgetError))
    }
}
// if checkOTB successfull  
export const checkOTB_FP = (obj) => async (dispatch) => {
    try {
        // dispatch(progressLoading());
        let response;
        response = OTB(obj);
        var data = await response;
        dispatch(otbSuccessful(data))
    } catch (error) {
        dispatch(userForgetPasswordError(error));
    }
};
// if phone exist successfull then direct 
export const checkPhoneExist_FP = (phone_number, country_code) => async (dispatch) => {
    try {
        // dispatch(progressLoading());
        let response;
        response = checkPhone({ phone_number, country_code });

        var data = await response;
        if (data === true) {
            dispatch(phoneExist_FP(true))
        } else {
            dispatch(phoneNotExist_FP(false))
        }


    } catch (error) {
        dispatch(userForgetPasswordError(error));
    }
};