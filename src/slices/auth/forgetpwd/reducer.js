import { createSlice } from "@reduxjs/toolkit";

export const initialState = {
  forgetSuccessMsg: null,
  forgetError: null,  
  loading: false,
  user: null,
  success: false,
  error: false,
  otb: ''
};

const forgotPasswordSlice = createSlice({
  name: "forgotpwd",
  initialState,
  reducers: {
      userForgetPasswordSuccess(state, action) {
          // state.forgetSuccessMsg = action.payload;
          state.user = action.payload;
          state.loading = false;
          state.success = true;
          // state.registrationError = null;
      },
      userForgetPasswordError(state, action) {
          // state.forgetError = action.payload
          state.user = null;
          state.loading = false;
          // state.registrationError = action?.payload;
          state.error = true;
      },
      progressLoading(state) {
        state.loading = true;
      },
      otbSuccessful(state, action) {
        state.otb = action.payload;
      },
      phoneExist_FP(state, action) {
        state.phoneExist = action.payload;
      },
      phoneNotExist_FP(state, action) {
        state.phoneExist = action.payload;
      },
      setIsSuccess_FP(state, action) {
        state.success = action.payload;
      },
  },
});

export const {
  userForgetPasswordSuccess,
  userForgetPasswordError,
  progressLoading,
  phoneExist_FP,
  phoneNotExist_FP,
  otbSuccessful,
  setIsSuccess_FP
} = forgotPasswordSlice.actions

export default forgotPasswordSlice.reducer;
