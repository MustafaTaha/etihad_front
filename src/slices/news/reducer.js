import { createSlice } from "@reduxjs/toolkit";
import { getNewsList, getNewsSingle } from './thunk';
export const initialState = {
    newsLists: [],
    newsDetails: {},
    error: {},
};


const NewsSlice = createSlice({
    name: 'NewsSlice',
    initialState,
    reducer: {
        // apiError(state, action) {
        //     state.error = "some thing went wrong";
        //     state.loading = true; 

        // },
        // apiDetailsSuccess(state, action) {
        //     state.newsDetails = action.payload;
        //     state.loading = false; 
        // },
    },
    extraReducers: (builder) => {
        builder.addCase(getNewsList.fulfilled, (state, action) => {
            state.newsLists = action.payload;
        });
        builder.addCase(getNewsList.rejected, (state, action) => {
            state.error = action.payload?.error || null;
        });
        builder.addCase(getNewsSingle.fulfilled, (state, action) => {
            state.newsDetails = action.payload;
        });
        builder.addCase(getNewsSingle.rejected, (state, action) => {
            state.error = action.payload?.error || null;
        });
    }
});

// export const { apiError, apiDetailsSuccess } = NewsSlice.actions;

export default NewsSlice.reducer;