import { createAsyncThunk } from "@reduxjs/toolkit";

//Include Both Helper File with needed methods
import {
    getNewsList as getNewsListApi, getNewsSingle as getNewsDetails
} from "../../helpers/backend_helper";

// import { apiDetailsSuccess, apiError } from "./reducer";

export const getNewsList = createAsyncThunk("news/getNewsList", async () => {
    try {
        const response = getNewsListApi();
        return response;
    } catch (error) {
        return error;
    }
});

export const getNewsSingle = createAsyncThunk("news/getNewsDetails", async (id) => {
    try {
        const response = getNewsDetails(id);
        return response;
    } catch (error) {
        return error;
    }
});
// export const getNewsDetails = (id) => async (dispatch) => {
//     try {
//         dispatch(progressLoading());
//         let response;

//         response = getNewsDetails(id);

//         var data = await response;
//         if (data) {
            
//             dispatch(apiDetailsSuccess(data))
//         }
//     } catch (error) {
//         // dispatch(apiError(error));
//     }
// };
