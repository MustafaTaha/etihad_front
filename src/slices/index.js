import { combineReducers } from "redux";
 
// Authentication
import LoginReducer from "./auth/login/reducer";
import RegisterReducer from "./auth/register/reducer";
import AccountReducer from "./auth/register/reducer";
import ForgetPasswordReducer from "./auth/forgetpwd/reducer";
import ChangePasswordReducer from "./auth/ChangePassword/reducer";

//Home
import Main_sliderReducer from "./home/main_slider/reducer";

//Project
import ProjectsReducer from "./projects/reducer";

//payment
import PaymentReducer from "./payment/reducer";

//News
import NewsReducer from "./news/reducer";


// API Key
import APIKeyReducer from "./apiKey/reducer";

const rootReducer = combineReducers({
    Login: LoginReducer,
    Register: RegisterReducer,
    Account: AccountReducer,
    ForgetPassword: ForgetPasswordReducer,
    ChangePassword: ChangePasswordReducer,
    APIKey: APIKeyReducer,
    Projects: ProjectsReducer,
    News: NewsReducer,
    Payment: PaymentReducer,
    Main_slider: Main_sliderReducer,

});

export default rootReducer;
