//Include Both Helper File with needed methods 
import { postPayment } from "../../helpers/backend_helper";

import { paymentSuccess, apiError, progressLoading } from "./reducer";


export const userPayment = (obj) => async (dispatch) => {
    try {
        dispatch(progressLoading());
        let response;

        response = postPayment(obj);

        var data = await response;

        if (data) {
            if (data.status === "SUCCESS") {
                window.open(`${data.message}`, '_blank', 'noreferrer');
                dispatch(paymentSuccess(data))
            } else {
                dispatch(apiError(error));
            }

        }
    } catch (error) {
        dispatch(apiError(error));
    }
};
