import { createSlice } from "@reduxjs/toolkit";

export const initialState = {
    payment: {},
    error: "", // for error message
    loading: false,
    errorMsg: false, // for error
};

const loginSlice = createSlice({
    name: "Payment",
    initialState,
    reducers: {
        apiError(state, action) {
            state.error = "some thing went wrong";
            state.loading = true;
            state.errorMsg = true;

        },
        paymentSuccess(state, action) {
            state.payment = action.payload;
            state.loading = false;
            state.errorMsg = false;
        },
        progressLoading(state) {
            state.loading = true;
        },
    },
});

export const { apiError, paymentSuccess,  progressLoading } = loginSlice.actions;

export default loginSlice.reducer;
