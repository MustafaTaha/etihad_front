import { createSlice } from "@reduxjs/toolkit";
import { getProjectList, getDonationmaster, getExternalProjects, getOrphanList, getHumanitarianProjects,getSmscodes } from './thunk';
export const initialState = {
    projectLists: [],
    externalProjectLists: [],
    orphanProjectLists: [],
    humanitarianProjectLists: [],
    error: {},
    donationmaster: {},
    smsCodeList: [],
    isLoading: false,
    isSMSLoading:false
};


const ProjectsSlice = createSlice({
    name: 'ProjectsSlice',
    initialState,
    reducer: {},
    extraReducers: (builder) => {
        //  Project List
        builder.addCase(getProjectList.fulfilled, (state, action) => {
            state.projectLists = action.payload;
            state.isLoading = false;
        });
        builder.addCase(getProjectList.rejected, (state, action) => {
            state.error = action.payload?.error || null;
            state.isLoading = false;

        });
        builder.addCase(getProjectList.pending, (state, action) => {
            state.isLoading = true;
            state.error = null;
        });

        // Donation master

        builder.addCase(getDonationmaster.fulfilled, (state, action) => {
            state.donationmaster = action.payload;
            state.isLoading = false;
        });
        builder.addCase(getDonationmaster.rejected, (state, action) => {
            state.error = action.payload?.error || null;
            state.isLoading = false;

        });
        builder.addCase(getDonationmaster.pending, (state, action) => {
            state.isLoading = true;
            state.error = null;
        });

        //sms codes 

        builder.addCase(getSmscodes.fulfilled, (state, action) => {
            state.smsCodeList = action.payload;
            state.isSMSLoading = false;
        });
        builder.addCase(getSmscodes.rejected, (state, action) => {
            state.error = action.payload?.error || null;
            state.isSMSLoading = false;

        });
        builder.addCase(getSmscodes.pending, (state, action) => {
            state.isSMSLoading = true;
            state.error = null;
        });

        //getExternalProjects

        builder.addCase(getExternalProjects.fulfilled, (state, action) => {
            state.externalProjectLists = action.payload;
            state.isLoading = false;
        });
        builder.addCase(getExternalProjects.rejected, (state, action) => {
            state.error = action.payload?.error || null;
            state.isLoading = false;

        });
        builder.addCase(getExternalProjects.pending, (state, action) => {
            state.isLoading = true;
            state.error = null;
        });

        //getOrphanList

        builder.addCase(getOrphanList.fulfilled, (state, action) => {
            state.orphanProjectLists = action.payload;
            state.isLoading = false;
        });
        builder.addCase(getOrphanList.rejected, (state, action) => {
            state.error = action.payload?.error || null;
            state.isLoading = false;

        });
        builder.addCase(getOrphanList.pending, (state, action) => {
            state.isLoading = true;
            state.error = null;
        });

        //getHumanitarianProjects

        builder.addCase(getHumanitarianProjects.fulfilled, (state, action) => {
            state.humanitarianProjectLists = action.payload;
            state.isLoading = false;
        });
        builder.addCase(getHumanitarianProjects.rejected, (state, action) => {
            state.error = action.payload?.error || null;
            state.isLoading = false;

        });
        builder.addCase(getHumanitarianProjects.pending, (state, action) => {
            state.isLoading = true;
            state.error = null;
        });
    }
});
export const { apiError, smsCodesSuccess } = ProjectsSlice.actions;

export default ProjectsSlice.reducer;