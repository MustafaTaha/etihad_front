import { createAsyncThunk } from "@reduxjs/toolkit";

//Include Both Helper File with needed methods
import {
    getProjectList as getProjectListApi,
    getDonationmaster as getDonationmasterApi,
    getExternalProjects as getExternalProjectsApi,
    getOrphanList as getOrphanListApi,
    getSmscodes   as getSmscodesApi,
    getHumanitarianProjects as getHumanitarianProjectsApi
} from "../../helpers/backend_helper"; 

export const getProjectList = createAsyncThunk("projects/getProjectList", async (type) => {
    try {
        const response = getProjectListApi(type);
        return response;
    } catch (error) {
        return error;
    }
});

export const getDonationmaster = createAsyncThunk("projects/getDonationmaster", async () => {
    try {
        const response = getDonationmasterApi();
        return response;
    } catch (error) {
        return error;
    }
});

export const getExternalProjects = createAsyncThunk("projects/getExternalProjects", async () => {
    try {
        const response = getExternalProjectsApi();
        return response;
    } catch (error) {
        return error;
    }
});

export const getOrphanList = createAsyncThunk("projects/getOrphanList", async (type) => {
    try {
        const response = getOrphanListApi(type);
        return response;
    } catch (error) {
        return error;
    }
});

export const getHumanitarianProjects = createAsyncThunk("projects/getHumanitarianProjects", async () => {
    try {
        const response = getHumanitarianProjectsApi();
        return response;
    } catch (error) {
        return error;
    }
});

export const getSmscodes = createAsyncThunk("projects/getSmscodes", async (obj) => {
    try {
        const response = getSmscodesApi(obj);
        return response;
    } catch (error) {
        return error;
    }
});
 


